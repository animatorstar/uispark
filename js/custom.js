var device = true;
$(document).ready(function() {
    /* Validation Start */
    $(".custCardA_Form input").bind("contextmenu", function(event) {
        return false;
    });
    $(".custCardA_Form").attr("autocomplete", "off");
    //  $("#mobileNumber").focus();
    $("#mobileNumber,  #enterOTP, #amount, #editable_amount, #outstandingAmount,#buyNowOTP").on("keypress", function(event) {
        if (event.shiftKey) {
            event.preventDefault();
        }
    });
    $("#mobileNumber, #amount, #editable_amount, #outstandingAmount, #preAltNumberNewChange ,#siMlost, #pincodeCity").on("input", function() {
        var reg = /^0+/gi;
        if (this.value.match(reg)) {
            this.value = this.value.replace(reg, '');
        }
    });


    $("#mobileNumber").on("keyup input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 10 && (!isNaN(tempVal)) && (event.type = "input")) {
            validNumber();
        } else if (tempVal.length > 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if ((this.value.length > 0 && this.value.length < 10) && (event.keyCode == 8 || event.keyCode == 46)) {
            invalidNumber();
        } else {
            invalidNumber();
            $("#textfield__error_mobileNo").css("display", "none");
            if ($(".mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $(".mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            if ($(".mobNumberInput #validationIcon").hasClass("validNumber")) {
                $(".mobNumberInput #validationIcon").removeClass("validNumber");
            }
        }


    });
    $("#mobileNumber").on("blur", function(event) {
        if (this.value.length < 10 && this.value.length > 0) {
            if (!($("#validationIcon").hasClass("invalidNumber"))) {
                $("#validationIcon").addClass("invalidNumber");
            }
        }
    });


    $("#editable_amount").on("keyup keydown", function(event) {

        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 8) || (event.keyCode == 46)) {

        } else {

            event.preventDefault();
        }

    });
    /* Postpaid Bill Amount Decimal Validation Start */
    $("#amount").on("input", function(event) {
        if ((!$(this).parent().siblings(".mobNumberInput").children("#validationIcon").hasClass("invalidNumber")) && ($(this).parent().siblings(".mobNumberInput").children("#mobileNumber").val() !== "")) {

            var val = this.value;
            if (val == "") {
                if (!$("#proceedLink").hasClass("mdl-button--disabled"))
                    $("#proceedLink").addClass("mdl-button--disabled");
            } else {
                if ($("#proceedLink").hasClass("mdl-button--disabled"))
                    $("#proceedLink").removeClass("mdl-button--disabled");
            }

            if (val.length > 8 && (val.indexOf(".") == -1 || val.indexOf(".") >= 8)) {
                $(this).val(val.substr(0, 8));
            }
            var re = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)$/g;
            var re1 = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)/g;
            if (re.test(val)) {} else {
                val = re1.exec(val);
                if (val) {
                    this.value = val[0];
                } else {
                    this.value = "";
                    if (!$("#proceedLink").hasClass("mdl-button--disabled"))
                        $("#proceedLink").addClass("mdl-button--disabled");
                }
            }
        } else {
            $(this).val("");
        }
    });

    /* Postpaid Bill Amount Decimal Validation End */

    $("#amount").on("focus", function() {
        if ($(this).parent().siblings(".mobNumberInput").children("#mobileNumber").val() == "")
            $(this).parent().siblings(".mobNumberInput").children("#validationIcon").addClass("invalidNumber");
    });

    /* Prepaid Amount Field Validation Start (uncomment to check)*/

    /*  $("#amount").on("input", function(event) {
    if ((!$(this).parent().siblings(".mobNumberInput").children("#validationIcon").hasClass("invalidNumber")) && ($(this).parent().siblings(".mobNumberInput").children("#mobileNumber").val() !== "")) {
    this.value = this.value.replace(/[^0-9]/g, '');
    if ((parseInt(this.value) !== 0) && (this.value !== "") && (/^\d+$/.test(parseInt(this.value)))) {
    if ($("#proceedLink").hasClass("mdl-button--disabled")) $("#proceedLink").removeClass("mdl-button--disabled");
    } else {
    $("#textfield__error_amount").css("display", "none");
    $("#textfield__error_amount").parent("div").removeClass("textfield__error");
    if (!($("#proceedLink").hasClass("mdl-button--disabled"))) $("#proceedLink").addClass("mdl-button--disabled");
    }
    } else {
    $(this).val("");
    }

    }); */
    /* Prepaid Amount Field Validation End */


    $("#outstandingAmount").on("input", function(event) {
        var val = this.value;
        if (val.length === 0) {
            if (!$("#pgOutstandingProceed").hasClass("mdl-button--disabled"))
                $("#pgOutstandingProceed").addClass("mdl-button--disabled");
        } else {
            if ($("#pgOutstandingProceed").hasClass("mdl-button--disabled"))
                $("#pgOutstandingProceed").removeClass("mdl-button--disabled");
        }

        if (val.length > 8 && (val.indexOf(".") == -1 || val.indexOf(".") >= 8)) {
            $(this).val(val.substr(0, 8));
        }
        var re = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)$/g;
        var re1 = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)/g;
        if (re.test(val)) {} else {
            val = re1.exec(val);
            if (val) {
                this.value = val[0];
            } else {
                this.value = "";
                if (!$("#pgOutstandingProceed").hasClass("mdl-button--disabled"))
                    $("#pgOutstandingProceed").addClass("mdl-button--disabled");
            }
        }
    });

    $("#enterOTP").on("input", function(event) {

        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 6 && (!isNaN(tempVal))) {
            // validOtp();

        } else if (tempVal.length > 6) {
            $(this).val($(this).val().substring(0, 6) + '');
        } else if (tempVal.length > 0 && tempVal.length < 10 && (event.keyCode == 8 || event.keyCode == 46)) {
            // inValidOtp();
        }

    });

    $("#requestOTP").click(function() {
        $("#enterOTP").removeAttr("disabled");
    });


    function validNumber() {
        $("#amount").val("");
        $("#amount").removeAttr("disabled");
        $("#amount").parent().removeClass("is-disabled");
        $("#textfield__error_mobileNo").css("display", "none");
        $(".mobNumberInput").removeClass("textfield__error");
        if ($(".mobNumberInput #validationIcon").hasClass("invalidNumber")) {
            $(".mobNumberInput #validationIcon").removeClass("invalidNumber");
        }
        if (!$(".mobNumberInput #validationIcon").hasClass("validNumber")) {
            $(".mobNumberInput #validationIcon").addClass("validNumber");
        }
        if (!($("#proceedLink").hasClass("mdl-button--disabled")))
            $("#proceedLink").addClass("mdl-button--disabled");
        if ($("#custCardA_validation a").hasClass("disabledLinks")) {
            $("#custCardA_validation a").removeClass("disabledLinks");
        }
        if (!($("#custCardA_validation a").hasClass("enabledLinks"))) {
            $("#custCardA_validation a").addClass("enabledLinks");
        }
        $("#custCardA_validation a").css("pointer-events", "auto");
    }

    function invalidNumber() {
        $("#amount").val("");
        $("#amount").attr("disabled", "disabled");
        $("#amount").parent().removeClass("is-dirty");
        $("#textfield__error_mobileNo").css("display", "block");
        $(".mobNumberInput").addClass("textfield__error");
        if (!$(".mobNumberInput #validationIcon").hasClass("invalidNumber")) {
            $(".mobNumberInput #validationIcon").addClass("invalidNumber");
        }
        if ($(".mobNumberInput #validationIcon").hasClass("validNumber")) {
            $(".mobNumberInput #validationIcon").removeClass("validNumber");
        }
        if (!($("#proceedLink").hasClass("mdl-button--disabled")))
            $("#proceedLink").addClass("mdl-button--disabled");
        if ($("#custCardA_validation a").hasClass("enabledLinks")) {
            $("#custCardA_validation a").removeClass("enabledLinks");
        }
        if (!($("#custCardA_validation a").hasClass("disabledLinks"))) {
            $("#custCardA_validation a").addClass("disabledLinks");
        }
        $("#custCardA_validation a").css("pointer-events", "none");
    }
    /* Validation End */

    /* Choose Your Payment Tab Show Start  */
    $(".tabStyle_custCardA").click(function(event) {
        event.preventDefault();
        $(".tabStyle_custCardA").each(function() {
            $(this).removeClass("active");

        });
        $(this).addClass("active");
        $("#proceedPaymentGateway").removeClass("mdl-button--disabled");
    });

    $(".tabStyle_custCardA a").click(function(event) {
        event.preventDefault();
        $(".tabStyle_custCardA").each(function() {
            $(this).removeClass("active");

        });
        $(this).parents("div.tabStyle_custCardA").addClass("active");
        $("#proceedPaymentGateway").removeClass("mdl-button--disabled");
    });
    /* Choose Your Payment Tab Show End */

    /* Choose Bank Option in Netbanking Start  */
    $(".buttonStyle_custCardA").click(function(event) {
        event.preventDefault();
        $(".buttonStyle_custCardA").each(function() {
            $(this).removeClass("active");
        });

        $(this).addClass("active");

    });
    /* Choose Bank Option in Netbanking End */

});

/* Tabs-Scrollable Start */
$(document).ready(function() {
    $('.uni-Tabs div').removeClass('mdl-layout__container');
    $('.mdl-layout__tab-bar-button').addClass('mdl-layout--small-screen-only');
});
/* Tabs-Scrollable End */


/* Header Desktop - Mobile Start */
$(document).ready(function() {
    /* Global-Header Start */
    if ($(window).width() >= 1024) {
        if ($("div.mdl-layout__drawer").has("div.global-header")) {

        } else {
            var $globalHeader = $(".global-header").clone();
            // var $selectLocation = $(".selectLocationWrapper").clone();
            $(".global-header").remove();
            //  $(".selectLocationWrapper").remove();
            $("body > div.mdl-layout").prepend($globalHeader);
            /* if (!$("div.global-header").has(".selectLocationWrapper")) {
            $("div.global-header div.global-header-links").append($selectLocation);
            }*/
            $(".global-header .selectLocationWrapper").addClass("fRight");
            //getmdlSelect.init(".selectLocation");
        }
    } else {
        if ($("body > div.mdl-layout").has("div.global-header")) {
            var $globalHeader = $(".global-header").clone();
            //  var $selectLocation = $(".selectLocationWrapper").clone();
            $(".global-header").remove();
            //  $("div.mdl-layout__drawer").prepend($selectLocation);
            $("div.mdl-layout__drawer").prepend($globalHeader);
            //  $(".global-header .selectLocationWrapper").remove();
            $(".global-header .selectLocationWrapper").removeClass("fRight");
            // getmdlSelect.init(".selectLocation");
        }
    }
    /* $(window).scroll(function() {
    if ($(window).scrollTop() >= 50) {
    $("header").addClass("fixed");

    } else {
    $("header").removeClass("fixed");
    }
    });*/
    /* Global-Header End */
    /* Header Navigation Start */
    var showHeader = function() {
        var url = window.location.pathname;
        var url_arr = url.split("/");
        var url_substr = "";
        if ($(window).width() >= 1024) {
            if ($("div.mdl-layout__drawer").has("div.left-nav")) {
                var $leftNav = $(".left-nav").clone();
                $(".left-nav").remove();
                $("header.mdl-layout__header > div.container > div.mdl-layout__header-row > div.mdl-layout-spacer").before($leftNav);
                if (!$(".subNav_links").parents().hasClass("subNav_links_wrapper")) {
                    $(".subNav_links").wrap("<div class='subNav_links_wrapper'></div>");
                    $(".subNav_links").wrap("<div class='container'></div>");
                }
                $(".left-nav li.subNav ul.subNav_links").removeClass("mdlext-collapsible-group");
                $(".left-nav li.subNav ul.subNav_links").removeAttr("hidden");
            }
        } else {
            if ($("header.mdl-layout__header > div.container > div.mdl-layout__header-row").has("div.left-nav")) {
                var $leftNav = $(".left-nav").clone();
                $(".left-nav").remove();

                $(".mdl-layout__drawer").append($leftNav);
                if ($(".subNav_links").parents().hasClass("subNav_links_wrapper")) {
                    $(".subNav_links").unwrap();
                    $(".subNav_links").unwrap();
                }
                $(".left-nav li.subNav ul.subNav_links").addClass("mdlext-collapsible-group");
                $(".left-nav li.subNav .subNav_links").attr("hidden", "");
            }
        }
        url_substr = url_arr[url_arr.length - 1];

        setDefaultActive(url_substr);
    };

    showHeader();

    $(".mdl-layout__drawer > .mdl-navigation > ul > li.subNav > a").click(function(event) {
        event.preventDefault();

        var collapsible_group = $(this).siblings(".mdlext-collapsible-group.subNav_links");
        var isHidden = collapsible_group.attr("hidden");

        if (typeof isHidden !== typeof undefined && isHidden !== false) {
            collapsible_group.removeAttr("hidden");
        } else {
            collapsible_group.attr("hidden", "");
        }

    });

    function setDefaultActive(url_substr) {

        var active_link = "";
        var active_subLink = "";
        active_link = $(".navbar-idea.left-nav ul li a[href='" + url_substr + "']");

        if ((active_link).closest("ul").hasClass("subNav_links")) {

            active_subLink = active_link;

            active_link = (active_subLink).closest("ul.subNav_links").parent("div").parent("div").siblings("a");
        } else {
            active_subLink = active_link.siblings("div").children("div").children(".subNav_links").children("li").first().children("a");

        }
        active_link.addClass("active");
        active_link.siblings("div").children("div").children(".subNav_links").css("display", "block");
        active_subLink.addClass("active");
        if ($(window).width() > 768) {
            $(".page-content").css("margin-top", "0px");
        } else {
            $(".page-content").css("margin-top", "0px");
        }
    }

    /* Orientation change of IPad Start */
    $(window).on("orientationchange", function() {
        showHeader();
    });
    /* Orientation change of IPad End */
    /* Header Navigation End */

    /* Search Overlay Start */
    $('#search_field').on('click', function(event) {
        event.preventDefault();
        $.fn.searchDialog();
    });
    $.fn.searchDialog = function() {

        var dialog = document.querySelector("dialog#search_overlay");

        if (!dialog.showModal) {
            dialogPolyfill.registerDialog(dialog);
        }
        dialog.showModal();
        document.querySelector('dialog#search_overlay .close').addEventListener('click', function() {
            // event.preventDefault();
            document.querySelector("dialog#search_overlay").close();
        });

    };

});
/* Search Overlay End */
/* Header Desktop - Mobile End */




/* Footer Toggle Start */
$(document).ready(function() {
    $(".new-footer-content").hide();
    $(".new-toggle-down").hide();
    $(".new-toggle-up").click(function() {
        $(".new-toggle-down").show();
        $(this).hide();
        $(".new-footer-content").slideDown(500);
        if ($(".new-footer-content:visible")) {
            $('html, body').animate({
                scrollTop: $(".new-footer-content").offset().top
            }, "slow");
        }
    });
    $(".new-toggle-down").click(function() {
        $(".new-footer-content").slideUp(500);
        $(this).hide();
        $(".new-toggle-up").show();
    });
});
/* Footer Toggle End */






/* Ellipses Start */
$(document).ready(function() {
    $(".ellipses1").text(function(index, currentText) {
        if (currentText.length > 100)
            return currentText.substr(0, 100) + '...';
    });
});
$(document).ready(function() {
    $(".ellipses2").text(function(index, currentText) {
        if (currentText.length > 80)
            return currentText.substr(0, 80) + '...';
    });
});
/* Ellipses End */

/* Promotion Carousel Start */
$(document).ready(function() {
    $('#promotion-carousel').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        swipe: true,
        touchMove: true,
        //dots: true,
        autoplay: false,


        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 2,
                    // dots: true,
                    infinite: false,
                    variableWidth: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 1,
                    //  dots: true,
                    infinite: false
                }
            }
        ]
    });
});
/* Promotion Carousel End */
/* Promotion Carousel1 Start */
$(document).ready(function() {
    $('#promotion-carousel1').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        swipe: true,
        touchMove: true,
        //dots: true,
        autoplay: false,

        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 2,
                    // dots: true,
                    infinite: false,
                    variableWidth: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 1,
                    //  dots: true,
                    infinite: false
                }
            }
        ]
    });
});
/* Promotion Carousel1 End */
/* Promotion Carousel Start */
$(document).ready(function() {
    $('#promotion-carousel').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        swipe: true,
        touchMove: true,
        //dots: true,
        autoplay: false,


        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 2,
                    // dots: true,
                    infinite: false,
                    variableWidth: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 1,
                    //  dots: true,
                    infinite: false
                }
            }
        ]
    });
});
/* Promotion Carousel End */
/* Promotion Carousel1 Start */
$(document).ready(function() {
    $('#promotion-carousel1').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        swipe: true,
        touchMove: true,
        //dots: true,
        autoplay: false,

        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 2,
                    // dots: true,
                    infinite: false,
                    variableWidth: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 1,
                    //  dots: true,
                    infinite: false
                }
            }
        ]
    });
});
/* Promotion Carousel1 End */
/* Prepaid Carousel with 3-Items and arrows in Desktop Start */
$(document).ready(function() {
    $('#prepaid-carousel, #recommendation-carousel, #recommendationsForYou, #specially-for-you-carousel, #voiceOffers, #last-3-recharges-carousel').slick({

        // Show

        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        swipe: true,
        touchMove: true,
        arrows: true,
        infinite: false,

        // Hide
        dots: false,
        autoplay: false,



        responsive: [{
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    variableWidth: true,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    variableWidth: true,
                    arrows: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true,
                    // Hide
                    arrows: false
                }
            }
        ]
    });
});
/* Prepaid Carousel with 3-Items and arrors in Desktop End */


$(document).ready(function() {
    $("#check-last-3-recharge").click(function() {
        $("#show-last-3-recharges").show();
        $(".closeDiv").show();
    });
    $(".closeDiv").click(function() {
        $("#show-last-3-recharges").hide();
        $(".closeDiv").hide();
    });

});

/* Last 3 Recharges End */

/* Last 3 Recharge Carousel End */


/* AppPromo-Banner Start */
$(document).ready(function() {
    $('#appPromo-Banner').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        swipe: true,
        touchMove: true,
        //dots: true,
        autoplay: false,


        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 2,
                    // dots: true,
                    infinite: false,
                    variableWidth: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 1,
                    //  dots: true,
                    infinite: false
                }
            }
        ]
    });
});


/* AppPromo-Banner End */


/* Treanding ,Trending Deals, Deals For You,Web App , Shopping , Entertainment, Restraunts with 4-Items and Arrows in Desktop Start */
$('#trending-carousel , #deals-for-you-carousel, #trending-deals-carousel , #web-app-exclusive-offers-carousel, #shopping-deals-carousel, #entertainment-deals-carousel, #deals-on-restraunts-carousel').slick({

    // Show
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    swipe: true,
    touchMove: true,
    arrows: true,
    infinite: false,

    // Hide
    dots: false,
    autoplay: false,



    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                variableWidth: true,
                arrows: false,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
                // Hide
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                // Hide
                arrows: false
            }
        }
    ]
});
/* Trending ,Trending Deals, Deals For You,Web App , Shopping , Entertainment ,Restraunts Carousel  with 4-Items and Arrows in Desktop End */

/* Top Banner Carousel with 1-Item and no-Arrows Start */
$('#top-banner-carousel, #homepage-topBanner').slick({
    // Show
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipe: true,
    touchMove: true,
    arrows: false,
    infinite: false,
    dots: true,
    autoplay: false,

    // Hide
    // autoplay: false,



    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                // variableWidth: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                // variableWidth: true,
                // Hide
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                // variableWidth: true,
                // Hide
                arrows: false
            }
        }
    ]
});
/* Top Banner Carousel with 1-Item and no-Arrows End */

/* Testimonial Carousel with 1-Item and Arrows Start */
$('#testimonial-Carousel').slick({
    // Show
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipe: true,
    touchMove: true,
    arrows: true,
    infinite: false,
    dots: true,
    autoplay: false,

    // Hide
    // autoplay: false,

    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                // variableWidth: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                // variableWidth: true,
                // Hide
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                // variableWidth: true,
                // Hide
                arrows: false
            }
        }
    ]
});
/*Testimonial Carousel with 1-Item and Arrows End */


/* Simple Carousel Start */
$(document).ready(function() {

    $('#roamingPack-carousel').slick({
        // Show 
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        swipe: true,
        touchMove: true,
        arrows: true,
        infinite: true,
        // Hide 
        dots: false,
        autoplay: false,
        // Next & Previous images
        prevArrow: "<img class='slick-prev' src='images/small-black-previous-arrow.png'>",
        nextArrow: "<img class='slick-next' src='images/small-black-next-arrow.png'>",

        responsive: [{
            breakpoint: 1025,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
                arrows: false,
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
                // Hide
                arrows: false
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                // Hide
                arrows: false
            }
        }]
    });

});

/* Simple Carousel End */


/*Range Slider Start*/
$(document).ready(function() {
    $(".addui-slider-handle").addClass("addui-slider-handle-active");
    $(window).mousemove(function(event) {
        $(".addui-slider-handle").addClass("addui-slider-handle-active");
    });

});
/*Range Slider End*/

/*Range Slider Start*/
$(document).ready(function() {
    $(window).mousemove(function(event) {
        $(".addui-slider-handle-l").addClass("addui-slider-handle-active");
        $(".addui-slider-handle-h").addClass("addui-slider-handle-active");
    });
});
/*Range Slider End*/

/* ###### Prepaid Screen 8 Start ###### */
$(document).ready(function() {
    $(".uni-TabScroll-Wrapper .slick-arrow").addClass("mdl-layout--desktop-screen-only");
});
/* ###### Prepaid Screen 8 End ###### */


/* Offers Dialog Script Start  */
$(document).ready(function() {
    $.fn.myDialog = function() {

        var dialog = document.querySelector('dialog#offer-dialog');
        if (!dialog.showModal) {
            dialogPolyfill.registerDialog(dialog);
        }

        dialog.showModal();
        dialog.querySelector('dialog#offer-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            dialog.close();
        });

        dialog.addEventListener('click', function(event) {
            var rect = dialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                dialog.close();
            }
        });

    };
    $('.dialogBtn').on('click', function() {
        $.fn.myDialog();
    });
});
/* Offers Dialog script End */


/* ###### Prepaid Screen 8 Start ###### */
$(document).ready(function() {
    $(".uni-TabScroll-Wrapper .slick-arrow").addClass("mdl-layout--desktop-screen-only");
});
/* ###### Prepaid Screen 8 End ###### */

/*#### Create number Start ####*/
/*####Select Series Start####*/
$(document).ready(function() {
    $(".special-3-box> ul>li").click(function() {
        $(this).addClass("createNumselected").siblings().removeClass("createNumselected");
    });
});

/*####Select Series End####*/
/*#### Create number End ####*/

/*#### Live Chat Start ####*/

$(document).ready(function() {
    $('.live-chat-btn').on("click", function() {
        // alert('hi');
        //  $(this).hide();
        $('.live-chat-content').show();

    });

});
//$(document).on('click', '', function(e) {
$('#live-chat-close').on("click", function() {
    $('.live-chat-content').hide();

});

/*#### Live Chat End ####*/


/*#### Bulk Bill Pay Start ####*/

$(document).ready(function() {
    var counter = 0;
    var amount = [];
    $(".uniqueCardA_wrapper input[type='checkbox']").on("change", function(event) {

        var accHolder = $(this).parents(".uniqueCardA_wrapper");
        // var accHolderName = accHolder.find(".accHolderName").text().split(" ")[1];
        var accHolderName = accHolder.find(".accHolderName").text();
        var dueAmount = accHolder.find(".dueAmount").html();

        if ($(this).attr("checked") === "checked") {
            counter++;
            if ($(".bulkBillPayWrapper").css('display') == 'none') {
                $(".bulkBillPayWrapper").css("display", "block");
            }

            if (counter > 3) {
                counter = 3;
                // alert("Maximum 3 accounts can be selected");
                $(this).parent().removeClass("is-checked");
                $(this).removeAttr("checked");
                $(this).prop("checked", false);
                $.fn.bulkPayMsgDialog();
                return false;
            } else {
                var addBillBlock;
                $(".addBillBlock").each(function() {
                    if ($(this).find("p.billAmount").text() == "") {
                        addBillBlock = $(this);
                        return false;
                    }
                });
                addBillBlock.find("p.accHolderName").text(accHolderName);
                addBillBlock.find("p.billAmount").html(dueAmount);
                addBillBlock.find("p.billAmount svg path").attr("fill", "#717171");
                addBillBlock.find("div.billAmtBlock").append("<a href='#' class='remove fRight'><span class=''> X </span></a>");
                //  if(device){
                //   console.log(device);
                if (!$('.bbpIcon').hasClass('open')) {
                    // console.log('###jkjkl###');
                    $('.bbpIcon').addClass('open');
                    // console.log('###jkjkl#1##');
                    $('.custHeadlineD').slideDown();
                    $('.billBlockWrapper').slideDown();
                    //console.log('###jkjkl2###');
                } else {
                    //    $('.billBlockWrapper').slideUp();
                }


                // }
                addBillBlock.find("a.remove").click(function(event) {
                    event.preventDefault();
                    var removeBillBlock;
                    counter--;
                    $(".uniqueCardA_wrapper").each(function() {
                        if ($(this).find(".accHolderName").text() === accHolderName) {
                            $(this).find("label.mdl-checkbox").removeClass("is-checked");
                            $(this).find("input[type='checkbox']").removeAttr("checked");
                            $(this).find("input[type='checkbox']").prop("checked", false);
                            removeBillBlock = addBillBlock;
                            return false;
                        }

                    });
                    var removeAmount = parseFloat(removeBillBlock.find(".billAmount span").text());
                    /* amount = $.grep(amount, function(value) {
                        return value != removeAmount;
                    }); */
                    for (var i in amount) {
                        if (amount[i] === removeAmount) {
                            amount.splice(i, 1);
                            break;
                        }
                    }
                    removeBillBlock.find(".accHolderName").text("Bill " + (removeBillBlock.index()));
                    removeBillBlock.find(".billAmount").html("");
                    removeBillBlock.find(".billAmtBlock").find(".remove").remove();
                    showHideBulkBillPay();
                    updateTotalAmount(amount);
                });
                amount.push(parseFloat(addBillBlock.find("p.billAmount span").text()));
                // if('.bbpIcon').hasClass()

            }

        } else if ($(this).attr("checked") === undefined) {
            counter--;
            var removeBillBlock;
            $(".addBillBlock").each(function() {
                if ($(this).find(".accHolderName").text() === accHolderName) {
                    removeBillBlock = $(this);
                    return false;
                }
            });

            var removeAmount = parseFloat(removeBillBlock.find(".billAmount span").text());
            /* amount = $.grep(amount, function(value) {
                return value != removeAmount;
            }); */
            for (var i in amount) {
                if (amount[i] === removeAmount) {
                    amount.splice(i, 1);
                    break;
                }
            }
            removeBillBlock.find(".accHolderName").text("Bill " + (removeBillBlock.index()));
            removeBillBlock.find(".billAmount").html("");
            removeBillBlock.find(".billAmtBlock").find(".remove").remove();
            showHideBulkBillPay();
        }

        updateTotalAmount(amount);
    });

    function showHideBulkBillPay() {
        var isAccountSelected = false;
        $(".uniqueCardA_wrapper").each(function() {
            if ($(this).find("label.mdl-checkbox").hasClass("is-checked")) {
                isAccountSelected = true;
            } else {}
        });
        if (isAccountSelected === false) {
            if ($(".bulkBillPayWrapper").css("display") == "block") {
                $(".bulkBillPayWrapper").css("display", "none");
            }
        }
    }

    function updateTotalAmount(amount) {
        var totalAmount = 0;
        for (var i = 0; i < amount.length; i++) {
            totalAmount += amount[i];
            //console.log("Total Amount = "+totalAmount);
        }
        $(".totalBillBlockWrapper p.billAmount span").text(totalAmount.toLocaleString('en-IN', {
            minimumFractionDigits: 2
        }));
    }
    $.fn.bulkPayMsgDialog = function() {
        var bulkPayDialog = document.querySelector("dialog#bulkPayMsg-dialog");

        if (!bulkPayDialog.showModal) {
            dialogPolyfill.registerDialog(bulkPayDialog);
        }
        bulkPayDialog.showModal();
        document.querySelector('dialog#bulkPayMsg-dialog .close').addEventListener('click', function() {
            // event.preventDefault();
            document.querySelector("dialog#bulkPayMsg-dialog").close();
        });

        /* document.querySelector('dialog#bulkPayMsg-dialog #bulkPayMsgDone').addEventListener('click', function() {
            document.querySelector("dialog#bulkPayMsg-dialog").close();
        }); */
    };
});

/*#### Bulk Bill Pay End ####*/


/* ReadMore Toggle Start */
$(".readMore-content").hide();
$(".readMore-toggle").show();
$(".readMore-toggle").click(function() {
    $(".toggle-down").show();
    $(this).hide();
    $(".readMore-content").slideDown(500);
    if ($(".readMore-content:visible")) {
        $('html, body').animate({
            scrollTop: $(".readMore-content").offset().top
        }, "slow");
    }
});
$(".toggle-down").click(function() {
    $(this).hide();
    $(".readMore-content").slideUp(500);
    $(".readMore-toggle").show(500);
});

/* ReadMore Toggle End */



/* ReadMore Popup Start */
$('.card_popUp').on('click', function(event) {
    event.preventDefault();
    $.fn.searchDialog();
});
$.fn.searchDialog = function() {

    var dialog = document.querySelector("dialog.card_popUp");

    if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }
    dialog.showModal();
    document.querySelector('dialog.card_popUp .close').addEventListener('click', function() {
        // event.preventDefault();
        document.querySelector("dialog.card_popUp").close();
    });

};

//popWrapper Start Here
$('.popUpWrapper .close').on('click', function() {
    $(this).parents(".mdl-menu__container").removeClass('is-visible');
});




/* ReadMore Popup End */


/*International Roaming Start*/
$('.packInfo').hide(); // Initially hide pack info
//On change of country dropdown show plans and packs
$('#interRomDropdown').change(function() {
    $('.packInfo').show();
    $('#roamingPack-carousel').slick('reinit');
});


/*International Roaming End*/

/*####Country search Start####*/
// Initialize autocomplete with local lookup:
/* $(function() {
$('#interRomDropdown').devbridgeAutocomplete({
// lookup: cities,
minChars: 1,
onSelect: function(suggestion) {

$('#selection').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
},
showNoSuggestionNotice: true,

noSuggestionNotice: 'Sorry, no matching results',
onInvalidateSelection: function() {


},
groupBy: 'category'
});
}); */
/*####Country search End####*/





/*###### DialogBox For Footer Social Icon #####*/
$.fn.confirmDialog = function() {

    var dialog = document.querySelector('dialog#confirm_dialog');
    if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }

    dialog.showModal();
    dialog.querySelector('dialog#confirm_dialog .close').addEventListener('click', function(event) {
        event.preventDefault();
        dialog.close();
    });

    dialog.addEventListener('click', function(event) {
        var rect = dialog.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        if (!isInDialog) {
            dialog.close();
        }
    });

};
$("#ab-logo, #fIcon,#lIcon, #gIcon").click(function(e) {
    e.preventDefault();
    $.fn.confirmDialog();
});

$.fn.feedbackDialog = function() {

    var dialog = document.querySelector('dialog#feedback_dialog');
    if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }

    dialog.showModal();
    dialog.querySelector('dialog#feedback_dialog .close , #dcancel').addEventListener('click', function(event) {
        event.preventDefault();
        dialog.close();
    });

    dialog.addEventListener('click', function(event) {
        var rect = dialog.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        if (!isInDialog) {
            dialog.close();
        }
    });

};
$("#tIcon").click(function(e) {
    e.preventDefault();
    $.fn.feedbackDialog();
});


if ($(window).width() < 768) {
    $(".popupOuterContr").css("position", "fixed");

}

/*###### DialogBox For Footer Social Icon End#####*/

$(document).ready(function() {
    /*#### Wallet SrollTop start #####*/
    if ($(window).width() < 768) {
        $("a[href='#wallet']").click(function(e) {
            // e.preventDefault();
            alert($($(this).attr("href")).offset().top);
            $("body, html").animate({
                scrollTop: $($(this).attr("href")).offset().top
            }, "slow");
            $($(this).attr("href")).focus();
        });
    }
    /*#### Wallet SrollTop end #####*/
    /*#### mobikwik code start #####*/
    $('#Mobikwik').on("click", function() {
        //	alert('mobikwik');
        $('.mobikwikContent').removeClass('hide');
        $(this).parent().addClass('active');
    });

    $('#ideaMoney, #paytm').on("click", function() {
        $('.mobikwikContent').addClass('hide');
        $(this).parent().addClass('active');
        $('.buttonStyle_custCardA').each(function() {
            if ($(this).hasClass('active'))
                $(this).removeClass('active');

        });
    });

    $("#mobikwikOTP").on("keyup input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 6 && (!isNaN(tempVal))) {
            // validMobikwikOtp();

        } else if (tempVal.length > 6) {
            $(this).val($(this).val().substring(0, 6) + '');
        } else if (tempVal.length > 0 && tempVal.length < 6 && (event.keyCode == 8 || event.keyCode == 46)) {
            // inValidMobikwikOtp();
        }

    });
    /*
    function validMobikwikOtp() {
    $("#OTPMessage").hide();
    $(".optInput #validationIcon").addClass("validNumber");
    $(".optInput #validationIcon").removeClass("invalidNumber");
    }

    function inValidMobikwikOtp() {
    $(".optInput #validationIcon").removeClass("validNumber");
    $(".optInput #validationIcon").addClass("invalidNumber");
    }*/
    /*##### mobikwik code End ######*/

    /*##### #####*/
    if ($('#carousel-bannerD').length > 0) {
        //alert('banner start')
        $('#carousel-bannerD').slick({
            // Show
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: true,
            touchMove: true,
            arrows: true,
            infinite: true,
            dots: true,

            // Hide
            autoplay: false,

            // Next & Previous images
            prevArrow: "<img class='slick-prev' src='../images/small-black-previous-arrow.png'>",
            nextArrow: "<img class='slick-next' src='../images/small-black-next-arrow.png'>",

            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // variableWidth: true
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // variableWidth: true,
                    // Hide
                    arrows: false
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    // variableWidth: true,
                    // Hide
                    arrows: false
                }
            }]
        });
    }
    /*##### #####*/

});

/*### Buy Now Journey Start ###*/
$(document).ready(function() {

    $("#buy_now #mobileNumber").on("input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 10 && (!isNaN(tempVal))) {
            if ($("#buy_now .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#buy_now .mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            if (!$("#buy_now .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#buy_now .mobNumberInput #validationIcon").addClass("validNumber");
            }
            $("#buyNow_confirm").removeClass("mdl-button--disabled");
        } else if (tempVal.length > 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if (tempVal.length > 0 && tempVal.length < 10 && (event.keyCode == 8 || event.keyCode == 46)) {
            if ($("#buy_now .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#buy_now .mobNumberInput #validationIcon").removeClass("validNumber");
            }
            if (!$("#buy_now .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#buy_now .mobNumberInput #validationIcon").addClass("invalidNumber");
            }
            $("#buyNow_confirm").addClass("mdl-button--disabled");
        } else {
            if ($("#buy_now .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#buy_now .mobNumberInput #validationIcon").removeClass("validNumber");
            }
            if ($("#buy_now .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#buy_now .mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            $("#buyNow_confirm").addClass("mdl-button--disabled");

        }


    });
    $("#buy_now #mobileNumber").on("blur", function(event) {
        if (this.value.length < 10 && this.value.length > 0) {
            if (!($("#buy_now .mobNumberInput #validationIcon").hasClass("invalidNumber"))) {
                $("#buy_now .mobNumberInput #validationIcon").addClass("invalidNumber");
            }
        }
    });


    $("#buyNowOTP").on("input", function(event) {

        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 4 && (!isNaN(tempVal))) {
            $("#prPasswprdProceed").removeClass("mdl-button--disabled");
        } else if (tempVal.length > 4) {
            $(this).val($(this).val().substring(0, 4) + '');
        } else if (tempVal.length > 0 && tempVal.length < 4 && (event.keyCode == 8 || event.keyCode == 46)) {
            $("#prPasswprdProceed").addClass("mdl-button--disabled");
        }

    });
    $("#buyNow_confirm").click(function(event) {
        event.preventDefault();
        $("#buy_now #mobileNumber").attr("disabled", "disabled");
        $("#buyNow_OTP").removeClass("hide");
        $("#mobileNo_confirm_action").addClass("hide");

    });
    $("#buyNow_otp_proceed").click(function(event) {
        event.preventDefault();
        /*  $("#planChange_successful").removeClass("hide");
        $("#buyNow_inner").addClass("hide"); */
        $("#planChange_successful").show();
        $("#buyNow_inner").hide();
    });
});

/*### Prepaid Packs Add-to-card Now Journey Start ###*/
$(document).ready(function() {
    $("#add_to_cart #mobileNumber").on("input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 10 && (!isNaN(tempVal))) {
            if ($("#add_to_cart .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#add_to_cart .mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            if (!$("#add_to_cart .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#add_to_cart .mobNumberInput #validationIcon").addClass("validNumber");
            }
            $("#add_to_cart_confirm").removeClass("mdl-button--disabled");
        } else if (tempVal.length > 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if (tempVal.length > 0 && tempVal.length < 10 && (event.keyCode == 8 || event.keyCode == 46)) {
            if ($("#add_to_cart .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#add_to_cart .mobNumberInput #validationIcon").removeClass("validNumber");
            }
            if (!$("#add_to_cart .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#add_to_cart .mobNumberInput #validationIcon").addClass("invalidNumber");
            }
            $("#add_to_cart_confirm").addClass("mdl-button--disabled");
        } else {
            if ($("#add_to_cart .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#add_to_cart .mobNumberInput #validationIcon").removeClass("validNumber");
            }
            if ($("#add_to_cart .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#add_to_cart .mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            $("#add_to_cart_confirm").addClass("mdl-button--disabled");
        }


    });
    $("#add_to_cart #mobileNumber").on("blur", function(event) {
        if (this.value.length < 10 && this.value.length > 0) {
            if (!($("#add_to_cart .mobNumberInput #validationIcon").hasClass("invalidNumber"))) {
                $("#add_to_cart .mobNumberInput #validationIcon").addClass("invalidNumber");
            }
        }
    });


    // $("#add_to_cartOTP").on("input", function(event) {

    //     var tempVal = $(this).val();
    //     this.value = this.value.replace(/[^0-9]/g, '');
    //     if (tempVal.length == 4 && (!isNaN(tempVal))) {
    //         $("#add_to_cart_otp_proceed").removeClass("mdl-button--disabled");
    //     } else if (tempVal.length > 4) {
    //         $(this).val($(this).val().substring(0, 4) + '');
    //     } else if (tempVal.length > 0 && tempVal.length < 4 && (event.keyCode == 8 || event.keyCode == 46)) {
    //         $("#add_to_cart_otp_proceed").addClass("mdl-button--disabled");
    //     }

    // });
    $("#add_to_cart_confirm").click(function(event) {
        event.preventDefault();
        $("#add_to_cart #mobileNumber").attr("disabled", "disabled");
        // $("#add_to_cart_OTP").removeClass("hide");
        $("#mobileNo_confirm_act").addClass("hide");
        document.querySelector('dialog#add_to_cart').close();

    });
    // $("#add_to_cart_otp_proceed").click(function(event) {
    //     event.preventDefault();
    //     $("#buyNow_inner").hide();
    // });
});


//TOGGLING NESTED ul
$(".user_drop-down .selected a").click(function() {
    $(".user_drop-down .options ul").toggle();
});

//SELECT OPTIONS AND HIDE OPTION AFTER SELECTION
$(".user_drop-down .options ul li a").click(function() {
    var text = $(this).html();
    console.log(text);
    $(".user_drop-down .selected a.sMList ").html(text);
    $(".user_drop-down .options ul").hide();
});


//HIDE OPTIONS IF CLICKED ANYWHERE ELSE ON PAGE
$(document).bind('click', function(e) {
    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("user_drop-down"))
        $(".user_drop-down .options ul").hide();
});


$(document).ready(function() {


    /* Horizontal Menu selected Menu Start*/
    var activeSelection = $(".horitanal_Menu_list li a.active").text();
    $('.current').html(activeSelection);
    $(".page_left_sideNav_list li a").on("click", function() {
        var currentSelection = $(this).html();
        $('.current').html(currentSelection);
    });


    $(".current").on("click", function() {
        $(this).parent().toggleClass('open');
        $(".horitanal_Menu_list").toggle();
        console.log('clicked');
    });

    if (screen.width < 982) {
        $(".btn-pos").on("click", function() {
            var position = $(this).attr("title");
            $('.current').text(position);
            $(".horitanal_Menu_list").toggle();
            $('.').toggleClass('open');
        });
    }


});




/* Usage Accordion Start */
$(document).ready(function() {

    $('.usagePacksSection .accordion_panels .accordion_panel:first-child .accordion_panel_content').css("display", "block");
    $('.usagePacksSection .accordion_panels .accordion_panel:first-child h2').addClass('active');
    $('.usagePacksSection .accordion_panels .accordion_panel:first-child h2').parent().addClass('accordion_panel_contentWhite');

    $('.usagePacksSection .accordion_panel h2').click(function() {

        if ($('.accordion_panels .accordion_panel h2').hasClass('active')) {

            $('.accordion_panels .accordion_panel h2').parent().removeClass('accordion_panel_contentWhite');
        }
        if ($(this).hasClass('active')) {
            // close panel if active and clicked
            $(this).removeClass('active').next().slideUp(200);
            $(this).parent().removeClass("accordion_panel_contentWhite");
        } else {


            // close all active panels        
            $('.accordion_panel h2.active').removeClass('active').next().slideUp(200);
            $(this).parent().addClass("accordion_panel_contentWhite");
            // open this clicked panel
            $(this).addClass('active')
                .next().slideDown(200);
        }

    });
});
/* Usage Accordion End */



$(document).ready(function() {

    /*#### Grid to list Start ####*/
    $('.viewType').on("click", function() {
        var typView = $(this).attr('data-val');
        $("#listGrid").removeClass().addClass(typView);
    });
    if ($(window).width() < 768) {
        $("#listGrid").removeClass().addClass("grid_view");

    }
    /*#### Grid to list End ####*/

    /* edit Account Start*/
    var eNameDialog;
    var allAcc_nickName;
    $('.eNameLink').on("click", function() {
        var en = $(this).closest('#listGrid .uniqueCardA_wrapper .cust_name').text();
        console.log(en);
        $.fn.editNameDialog();

    });
    $.fn.editNameDialog = function() {
        // alert('removed Called');
        $('dialog#editName-dialog .dialogM_innnerWrap').find('.dialogMTitle').show();
        $('dialog#editName-dialog .eName_firstBlock').show();
        $('dialog#editName-dialog .eName_secondBlock').hide();
        $('dialog#editName-dialog .eName_thirdBlock').hide();
        eNameDialog = document.querySelector('dialog#editName-dialog');
        if (!eNameDialog.showModal) {
            dialogPolyfill.registerDialog(eNameDialog);
        }
        eNameDialog.showModal();
        eNameDialog.querySelector('dialog#editName-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            eNameDialog.close();
        });


        eNameDialog.addEventListener('click', function(event) {
            var rect = eNameDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                eNameDialog.close();
            }
        });

    };

    /* DialogM  Remove Account code End*/
    $("#eNameProceed").on("click", function() {
        $('dialog#editName-dialog .eName_firstBlock').hide();
        allAcc_nickName = $('#allAcc_nickName').val();
        $('dialog#editName-dialog .eName_secondBlock').show();
        $('#allAcc_nickNameNew').html(allAcc_nickName);
    });

    $("#eNameOTPProceed").on("click", function() {
        $(this).closest('.dialogM_innnerWrap').find('.dialogMTitle').hide();
        $('dialog#editName-dialog .eName_thirdBlock').show();
        $('dialog#editName-dialog .eName_secondBlock').hide();
    });

    $("#editNameDone").on("click", function() {
        eNameDialog.close();
    });

    $("#removeAccount_Done, #removeAccount_Cancel").on("click", function() {
        removeDialog.close();
    });


    /* edit Account End*/

    /* DialogM  Remove Account code Start*/
    var removeDialog;
    $('.removeAccountLink a').on("click", function() {
        $.fn.removeAccountDialog();

    });
    $.fn.removeAccountDialog = function() {
        // alert('removed Called');
        $('dialog#removeAccount-dialog .removeAccount_FirstPopUP').show();
        $('dialog#removeAccount-dialog .removeAccount_SecondPopUP').hide();
        removeDialog = document.querySelector('dialog#removeAccount-dialog');
        if (!removeDialog.showModal) {
            dialogPolyfill.registerDialog(removeDialog);
        }

        removeDialog.showModal();
        removeDialog.querySelector('dialog#removeAccount-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            removeDialog.close();
        });


        removeDialog.addEventListener('click', function(event) {
            var rect = removeDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                removeDialog.close();
            }
        });

    };


    $("#removeAccount_confirm").on("click", function() {
        $(this).closest('.dialogM_innnerWrap').find('.dialogMTitle').hide();
        //$('.dialogMTitle').hide();
        $('dialog#removeAccount-dialog .removeAccount_FirstPopUP').hide();
        $('dialog#removeAccount-dialog .removeAccount_SecondPopUP').show();
    });
    $("#removeAccount_Done, #removeAccount_Cancel").on("click", function() {
        removeDialog.close();
    });
    /* DialogM  Remove Account code End*/
    /*  Plan & Services Apply code Start*/
    var removeDialog;
    $('.applyPlanLink').on("click", function() {
        $.fn.applyPlanDialog();

    });
    $.fn.applyPlanDialog = function() {
        // alert('removed Called');
        $('.applyPlan_FirstPopUP').show();
        $('.applyPlan_SecondPopUP').hide();
        removeDialog = document.querySelector('dialog#applyPlan-dialog');
        if (!removeDialog.showModal) {
            dialogPolyfill.registerDialog(removeDialog);
        }

        removeDialog.showModal();
        removeDialog.querySelector('dialog#applyPlan-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            removeDialog.close();
        });


        removeDialog.addEventListener('click', function(event) {
            var rect = removeDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                removeDialog.close();
            }
        });

    };


    $("#applyPlan_confirm").on("click", function() {
        $('.applyPlan_FirstPopUP').hide();
        $('.applyPlan_SecondPopUP').show();
    });
    $("#applyPlan_Done, #applyPlan_Cancel").on("click", function() {
        removeDialog.close();
    });
    /* Plan & Services Apply code End*/



    /*  Deactivate code Start*/
    jQuery.fn.center = function() {
        if ($(window).width() >= 768 && (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1) || navigator.userAgent.indexOf("Firefox") != -1)) {
            $("header.mdl-layout__header").css("z-index", "1");
            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop() - 350) + "px");
            return this;
        }
    }
    var removeDialog;
    $('.deactivatevasLink').on("click", function() {
        $.fn.deactivatePlanDialog();
    });
    $.fn.deactivatePlanDialog = function() {
        $('.deactivatePlan_FirstPopUP').show();
        $('.deactivatePlan_SecondPopUP').hide();
        removeDialog = document.querySelector('dialog#deactivate-dialog');
        if (!removeDialog.showModal) {
            dialogPolyfill.registerDialog(removeDialog);
        }
        $('dialog#deactivate-dialog').center();
        removeDialog.showModal();
        removeDialog.querySelector('dialog#deactivate-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            removeDialog.close();
            $(".mdl-layout__header").css("z-index", "3");
        });
        removeDialog.addEventListener('click', function(event) {
            var rect = removeDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                removeDialog.close();
                $(".mdl-layout__header").css("z-index", "3");
            }
        });
    };
    $("#deactivatePlan_confirm").on("click", function() {
        $('.deactivatePlan_FirstPopUP').hide();
        $('.deactivatePlan_SecondPopUP').show();
    });
    $("#deactivatePlan_Done, #deactivatePlan_Cancel").on("click", function() {
        removeDialog.close();
        $(".mdl-layout__header").css("z-index", "3");
    });
    /* Deactivate code End*/

    /*  Activate code Start*/

    var removeDialog1;
    $('.activatevasLink').on("click", function() {
        $.fn.activatePlanDialog();
    });
    $.fn.activatePlanDialog = function() {
        // alert('removed Called');
        $('.activatePlan_FirstPopUP').show();
        $('.activatePlan_SecondPopUP').hide();
        removeDialog1 = document.querySelector('dialog#activate-dialog');
        if (!removeDialog1.showModal) {
            dialogPolyfill.registerDialog(removeDialog1);
        }
        $('dialog#activate-dialog').center();
        removeDialog1.showModal();
        removeDialog1.querySelector('dialog#activate-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            removeDialog1.close();
            $(".mdl-layout__header").css("z-index", "3");
        });
        removeDialog1.addEventListener('click', function(event) {
            var rect = removeDialog1.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                removeDialog1.close();
                $(".mdl-layout__header").css("z-index", "3");
            }
        });
    };
    $("#activatePlan_confirm").on("click", function() {
        $('.activatePlan_FirstPopUP').hide();
        $('.activatePlan_SecondPopUP').show();
    });
    $("#activatePlan_Done, #activatePlan_Cancel").on("click", function() {
        removeDialog1.close();
        $(".mdl-layout__header").css("z-index", "3");
    });
    /* Activate code End*/


    /* Add Account DialogM Code Start*/
    var addAccountDialog;
    $('.addAccount').on("click", function() {
        $.fn.myAccountDialog();
    });

    $.fn.myAccountDialog = function() {
        $('dialog#addAccount-dialog .add_acc_dialog_first').show();
        $('dialog#addAccount-dialog .dialogM_innnerWrap').find('.dialogMTitle').show();
        $('dialog#addAccount-dialog .add_acc_dialog_second').hide();
        $('dialog#addAccount-dialog .add_acc_dialog_third').hide();
        addAccountDialog = document.querySelector('dialog#addAccount-dialog');
        if (!addAccountDialog.showModal) {
            dialogPolyfill.registerDialog(addAccountDialog);
        }

        addAccountDialog.showModal();
        addAccountDialog.querySelector('dialog#addAccount-dialog .close,').addEventListener('click', function(event) {
            event.preventDefault();
            addAccountDialog.close();
        });


        addAccountDialog.addEventListener('click', function(event) {
            var rect = addAccountDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                addAccountDialog.close();
            }
        });

    };


    $("#add_acc_proceed1").on("click", function() {
        $('dialog#addAccount-dialog .add_acc_dialog_first').hide();
        $('dialog#addAccount-dialog .add_acc_dialog_second').show();
        $('dialog#addAccount-dialog .add_acc_info').show();

    });
    $("#add_acc_proceed2").on("click", function() {
        $(this).closest('.dialogM_innnerWrap').find('.dialogMTitle').hide();
        $('dialog#addAccount-dialog .add_acc_dialog_third').show();
        $('dialog#addAccount-dialog .add_acc_dialog_second').hide();

    });
    $("#addAccountDialog_Done").on("click", function() {

        addAccountDialog.close();
        $(".AddUser").clone().removeClass('addUser').appendTo("#listGrid");
    });
    $("#addAccountDialog_Cancel").on("click", function() {
        addAccountDialog.close();
    });
    /* DialogM  Edit name code End */


    /* Activate Booster code Start*/
    var boosterDialog;
    $('.subscribe_Booster').on("click", function() {
        $.fn.boosterAccountDialog();
    });
    $.fn.boosterAccountDialog = function() {
        $('.boosterPack_secondPopup').hide();
        $('.boosterPack_firstPopup').show();
        boosterDialog = document.querySelector('dialog#activateBooster-dialog');
        if (!boosterDialog.showModal) {
            dialogPolyfill.registerDialog(boosterDialog);
        }
        boosterDialog.showModal();
        boosterDialog.querySelector('dialog#activateBooster-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            boosterDialog.close();
        });


        // boosterDialog.addEventListener('click', function(event) {
        //     var rect = boosterDialog.getBoundingClientRect();
        //     var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        //     if (!isInDialog) {
        //         // alert(isInDialog);
        //         boosterDialog.close();
        //     }
        // });

    };

    $("#boosterPack_confirm").on("click", function() {
        $('.boosterPack_secondPopup').show();
        $('.boosterPack_firstPopup').hide();
    });

    $("#boosterPack_done, #boosterPack_cancel").on("click", function() {
        boosterDialog.close();
    });
    /* Activate Booster code End*/



    /* Apply Emergency Recharge JS Start */
    $("#applyEmerRecharge").click(function() {
        $.fn.applyEmerRechargeDialog();
    });
    $.fn.applyEmerRechargeDialog = function() {
        $(".emerRecharge_firstPopup").show();
        $(".emerRecharge_secondPopup").hide();
        var applyEmerRechargeDialog = document.querySelector('dialog#applyEmerRecharge-dialog');
        if (!applyEmerRechargeDialog.showModal) {
            dialogPolyfill.registerDialog(applyEmerRechargeDialog);
        }
        applyEmerRechargeDialog.showModal();
        applyEmerRechargeDialog.querySelector('dialog#applyEmerRecharge-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            applyEmerRechargeDialog.close();
        });
        applyEmerRechargeDialog.querySelector('dialog#applyEmerRecharge-dialog #emerRecharge_Cancel').addEventListener('click', function(event) {
            event.preventDefault();
            applyEmerRechargeDialog.close();
        });

        applyEmerRechargeDialog.addEventListener('click', function(event) {
            var rect = applyEmerRechargeDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                applyEmerRechargeDialog.close();
            }
        });
        applyEmerRechargeDialog.querySelector('dialog#applyEmerRecharge-dialog #emerRecharge_Proceed').addEventListener('click', function(event) {
            $(".emerRecharge_firstPopup").hide();
            $(".emerRecharge_secondPopup").show();
        });
        applyEmerRechargeDialog.querySelector('dialog#applyEmerRecharge-dialog #emerRecharge_done').addEventListener('click', function(event) {
            event.preventDefault();
            applyEmerRechargeDialog.close();
        });
    };
    /* Apply Emergency Recharge JS Start */

});


/* idea Magic FAQ accrodion Start */

/* Usage Accordion Start */
$(document).ready(function() {

    $('.ideaMagicFAQ .accordion_panels .accordion_panel:first-child .accordion_panel_content').css("display", "block");
    $('.ideaMagicFAQ .accordion_panels .accordion_panel:first-child h2').addClass('active');
    $('.ideaMagicFAQ .accordion_panels .accordion_panel:first-child h2').parent().addClass('accordion_panel_contentWhite');

    $('.ideaMagicFAQ .accordion_panel h2').click(function() {

        if ($('.accordion_panels .accordion_panel h2').hasClass('active')) {

            $('.accordion_panels .accordion_panel h2').parent().removeClass('accordion_panel_contentWhite');
        }
        if ($(this).hasClass('active')) {
            // close panel if active and clicked
            $(this).removeClass('active').next().slideUp(200);
            $(this).parent().removeClass("accordion_panel_contentWhite");
        } else {


            // close all active panels        
            $('.accordion_panel h2.active').removeClass('active').next().slideUp(200);
            $(this).parent().addClass("accordion_panel_contentWhite");
            // open this clicked panel
            $(this).addClass('active')
                .next().slideDown(200);
        }

    });
});
/* Usage Accordion End */
/* idea Magic FAQ accordion End */

/* PinNumber Dropdown Open on Tab Click Start */
$(document).ready(function() {
    $('select#pinNumber').focus(function() {
        var myDropDown = $("#pinNumber");
        var length = $('#pinNumber> option').length;
        //open dropdown
        myDropDown.attr('size', 5);
        if ($(window).width() > 768) {
            // alert(">768");
            $('#pinNumber').css('position', 'absolute');
        }
        if ($(window).width() >= 320) {
            // alert(">768");
            $('#pinNumber').css('z-index', '99999');
            $('#pinNumber').css('position', 'absolute');
        }
    }).blur(function() {
        var myDropDown = $("#pinNumber");
        var length = $('#pinNumber> option').length;
        //close dropdown
        myDropDown.attr('size', 0);
    });
    /*  PinNumber Dropdown Open on Tab Click  End */


    /* international Roaming code Start*/
    var roamingDialog;
    $('input#roaming-switch').on("change", function() {
        $.fn.internationalRoamingDialog();
        if ($('#roaming-text').is('.is-checked') === false) {

            $("div#roaming-switch-label").text("ON");
        } else {

            $("div#roaming-switch-label").text("OFF");
        }
    });

    $.fn.internationalRoamingDialog = function() {
        $('.internationalRoaming_secondPopup').hide();
        $('.internationalRoaming_firstPopup').show();
        roamingDialog = document.querySelector('dialog#internationalRoaming-dialog');
        if (!roamingDialog.showModal) {
            dialogPolyfill.registerDialog(roamingDialog);
        }
        roamingDialog.showModal();
        roamingDialog.querySelector('dialog#internationalRoaming-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            roamingDialog.close();
        });

        $("dialog#internationalRoaming-dialog").on("click", function(event) {

            var rect = this.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            console.log(rect.height + "width" + rect.width);


            if (!isInDialog) {
                roamingDialog.close();
            } else {
                roamingDialog = document.querySelector('dialog#internationalRoaming-dialog');
                if (!roamingDialog.showModal) {

                    roamingDialog.close();
                }
            }
        });
    };

    $("#internationalRoaming_confirm").on("click", function() {

        $('.internationalRoaming_firstPopup').hide();
        $('.internationalRoaming_secondPopup').show();
        $("p#roaming-msg1").css("display", "none");
        $("p#roaming-msg2").css("display", "block");

        $('#roaming-switch').attr('disabled', 'disabled');

        setInterval(function() {
            $('#roaming-switch').prop('disabled', false);
            $("p#roaming-msg2").css("display", "none");
            $("p#roaming-msg1").css("display", "block");
        }, 30000);
    });

    $("#internationalRoaming_done, #internationalRoaming_cancel").on("click", function() {

        if (roamingDialog.showModal) {
            roamingDialog.close();
        }
    });

    /* international Roaming code End*/

    /* isd code Start*/
    var isdDialog;
    $('input#isd-switch').change("click", function() {
        $.fn.internationalISDDialog();
        if ($('#isd-text').is('.is-checked') === false) {
            $("div#isd-switch-label").text("ON");
        } else {
            $("div#isd-switch-label").text("OFF");
        }
    });

    $.fn.internationalISDDialog = function() {
        $('.isd_secondPopup').hide();
        $('.isd_firstPopup').show();
        isdDialog = document.querySelector('dialog#internationalISD-dialog');
        if (!isdDialog.showModal) {
            dialogPolyfill.registerDialog(isdDialog);
        }
        isdDialog.showModal();
        isdDialog.querySelector('dialog#internationalISD-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            isdDialog.close();
        });

        $("dialog#internationalISD-dialog").on("click", function(event) {

            var rect = this.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            console.log(rect.height + "width" + rect.width);


            if (!isInDialog) {
                isdDialog.close();
            } else {
                isdDialog = document.querySelector('dialog#internationalISD-dialog');
                if (!isdDialog.showModal) {

                    isdDialog.close();
                }
            }
        });
    };

    $("#isd_confirm").on("click", function() {

        $('.isd_firstPopup').hide();
        $('.isd_secondPopup').show();
        $("p#isd-msg").css("display", "block");
        $('#isd-switch').attr('disabled', 'disabled');
        setInterval(function() {
            $('#isd-switch').prop('disabled', false);
            $("p#isd-msg").css("display", "none");
        }, 30000);
    });

    $("#isd_done, #isd_cancel").on("click", function() {

        if (isdDialog.showModal) {
            isdDialog.close();
        }
    });

    /* isd code End*/

    /* Call Conference code Start*/
    var CCDialog;
    $('input#cc-switch').change("click", function() {
        $.fn.internationalCCDialog();
        if ($('#cc-text').is('.is-checked') === false) {
            $("div#cc-switch-label").text("ON");
        } else {
            $("div#cc-switch-label").text("OFF");
            //      $('#cc-switch').attr('disabled', 'disabled'); 
            //   setInterval(function(){  
            //             $('#cc-switch').prop('disabled', false);      

            //             }, 30000);   

        }
    });

    $.fn.internationalCCDialog = function() {
        $('.cc_secondPopup').hide();
        $('.cc_firstPopup').show();
        CCDialog = document.querySelector('dialog#internationalCC-dialog');
        if (!CCDialog.showModal) {
            dialogPolyfill.registerDialog(CCDialog);
        }
        CCDialog.showModal();
        CCDialog.querySelector('dialog#internationalCC-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            CCDialog.close();
        });

        $("dialog#internationalCC-dialog").on("click", function(event) {

            var rect = this.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            console.log(rect.height + "width" + rect.width);


            if (!isInDialog) {
                CCDialog.close();
            } else {
                CCDialog = document.querySelector('dialog#internationalCC-dialog');
                if (!CCDialog.showModal) {

                    CCDialog.close();
                }
            }
        });
    };

    $("#cc_confirm").on("click", function() {

        $('.cc_firstPopup').hide();
        $('.cc_secondPopup').show();
        $("p#cc-msg").css("display", "block");
        $('#cc-switch').attr('disabled', 'disabled');

        setInterval(function() {
            $('#cc-switch').prop('disabled', false);
            $("p#cc-msg").css("display", "none");
        }, 30000);
    });

    $("#cc_done, #cc_cancel").on("click", function() {

        if (CCDialog.showModal) {
            CCDialog.close();
        }
    });

    /* Call Conference code End*/

    /* Missed Call Alert code Start*/
    var MCADialog;
    $('input#mca-switch').change("click", function() {
        $.fn.internationalMCADialog();
        if ($('#mca-text').is('.is-checked') === false) {
            $("div#mca-switch-label").text("ON");
        } else {
            $("div#mca-switch-label").text("OFF");
        }
    });

    $.fn.internationalMCADialog = function() {
        $('.mca_secondPopup').hide();
        $('.mca_firstPopup').show();
        MCADialog = document.querySelector('dialog#internationalMCA-dialog');
        if (!MCADialog.showModal) {
            dialogPolyfill.registerDialog(MCADialog);
        }
        MCADialog.showModal();
        MCADialog.querySelector('dialog#internationalMCA-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            MCADialog.close();
        });

        $("dialog#internationalMCA-dialog").on("click", function(event) {

            var rect = this.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            console.log(rect.height + "width" + rect.width);


            if (!isInDialog) {
                MCADialog.close();
            } else {
                MCADialog = document.querySelector('dialog#internationalMCA-dialog');
                if (!MCADialog.showModal) {

                    MCADialog.close();
                }
            }
        });
    };

    $("#mca_confirm").on("click", function() {

        $('.mca_firstPopup').hide();
        $('.mca_secondPopup').show();
        $("p#mca-msg").css("display", "block");
        $('#mca-switch').attr('disabled', 'disabled');

        setInterval(function() {
            $('#mca-switch').prop('disabled', false);
            $("p#mca-msg").css("display", "none");
        }, 30000);
    });

    $("#mca_done, #mca_cancel").on("click", function() {

        if (MCADialog.showModal) {
            MCADialog.close();
        }
    });

    /* Missed Call Alert code End*/
    /* Ebill dialog code Start*/
    var ebillDialog;
    $('#ebillLink').on("click", function() {
        $.fn.ebillDialog();
    });
    $.fn.ebillDialog = function() {

        ebillDialog = document.querySelector('dialog#ebill-dialog');
        if (!ebillDialog.showModal) {
            dialogPolyfill.registerDialog(ebillDialog);
        }
        ebillDialog.showModal();
        ebillDialog.querySelector('dialog#ebill-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            ebillDialog.close();
        });
        ebillDialog.addEventListener('click', function(event) {
            var rect = ebillDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            console.log(rect.height + "width" + rect.width);
            console.log(isInDialog);
            if (!isInDialog) {
                ebillDialog.close();
            }
        });

    };



    $("#ebill_confirm, #ebill_Cancel").on("click", function() {
        ebillDialog.close();
    });
    /* Ebill dialog code Start*/
});
$(document).ready(function() {
    var req_con;
    var req_dd;
    var com_dd;
    $('.req_com').on("click", function() {
        //alert($(this).attr("for"));
        $('.requestsDD option:first').prop('selected', true);
        $('#complaints .dropdownSelect option:first').prop('selected', true);

        $(".request_error_msg").hide();
        req_con = $(this).attr("for");
        $(".manage_dropdown .universalDropdown").hide();
        $(".manage_dropdown ." + req_con + "DD").show();





    });
    $("#requestDropdown").change(function() {
        req_dd = $('#requestDropdown').find(":selected").text();
        $(".request_error_msg").hide();
        //alert(conceptName);
    });


    $("#complaintsDropdown").change(function() {
        com_dd = $('#complaintsDropdown').find(":selected").text();
        $(".request_error_msg").hide();
        //alert("Complainets");
    });








    /* Complaints Coverage JS Start */
    $.fn.preCoverageDialog = function() {

        var pCoverageDialog = document.querySelector('dialog#prepaid_coverage');
        if (!pCoverageDialog.showModal) {
            dialogPolyfill.registerDialog(pCoverageDialog);
        }
        pCoverageDialog.showModal();
        pCoverageDialog.querySelector('dialog#prepaid_coverage .close').addEventListener('click', function(event) {
            event.preventDefault();
            pCoverageDialog.close();
        });

        $('dialog#prepaid_coverage #preCov_cancel').click(function(event) {
            event.preventDefault();
            pCoverageDialog.close();
        });


    };


    // $("#pre_coverage").on("click", function() {
    //     alert("Form checking ");
    //     // $.fn.preCoverageDialog();
    // });

    $("#pre_coverageCancel").click(function(event) {
        window.location = "pre-my-request.html";
    });

    /* Complaints Coverage JS End */











    /*Sim Block Request Start*/

    var simRequest;
    $('#myRequstChanges').on("click", function() {
        // $.fn.simRequestIdDialog();
        // alert("Hello Is sime");

        if ($("#requestDropdown").val() == 0) {
            console.log("requestDropdown length is counted");
            $(".request_error_msg").show();
        } else {
            if ((req_con == 'requests') && (req_dd == 'Safe custody')) {
                $.fn.simRequestIdDialog();
                // console.log('Safe custody');
            } else if ((req_con == 'requests') && (req_dd == 'Do Not Distrub')) {
                if ($("#dndActivation-dialog .statusofDialog").text() == "A") {
                    alert("Hello Dnd Success");
                    $.fn.dndSuccessDialog();
                } else {
                    alert("dndActivationDialog");
                    $.fn.dndActivationDialog();
                }
            } else if ((req_con == 'requests') && (req_dd == 'Bill not received')) {
                $.fn.billNotReceivedDialog();
            } else if ((req_con == 'requests') && (req_dd == 'Update GSTIN')) {
                console.log("update GSTIN");
                $.fn.updateGSTINDialogs();
            } else if ((req_con == 'requests') && (req_dd == 'Handset/SIM lost')) {
                console.log("Handset/SIM lost");
                $.fn.simRequestIdDialog();
            } else if ((req_con == 'requests') && (req_dd == 'Prepaid statement on mail')) {
                console.log("Prepaid statement on mail");
                $.fn.prepaidStatementDialog();
            } else if ((req_con == 'requests') && (req_dd == 'MGM lead- WF awaited')) {
                console.log("MGM lead- WF awaited");
                $.fn.mgmLeadDialogs();
            } else if ((req_con == 'requests') && (req_dd == 'DND deactivation')) {
                console.log("DND deactivation");
                $.fn.DeactivationDialogs();
            }
        }




    });


    var simComplaints;
    $('#myRequstChanges').on("click", function() {
        if ((req_con == 'complaints') && (com_dd == 'Bill not received')) {
            //console.log('EBill not received');
            // $.fn.cBillNotReceivedDialog();

        }

    });


    $.fn.simRequestIdDialog = function() {
        //alert('removed Called');

        setTimeout(function() {
            console.log("simRequestIdDialog");
            $("dialog#simRequest #mobileNumber").focus();
        }, 10);


        simRequest = document.querySelector('dialog#simRequest');
        if (!simRequest.showModal) {
            dialogPolyfill.registerDialog(simRequest);
        }

        resetsimRequestDialog();
        simRequest.showModal();
        simRequest.querySelector('dialog#simRequest .close').addEventListener('click', function(event) {
            event.preventDefault();
            simRequest.close();
        });

        simRequest.addEventListener('click', function(event) {
            var rect = simRequest.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                simRequest.close();
            }
        });

    };


    $("#simLost_confirm").on("click", function() {
        $(".simLost_FirstPopUP").hide();
        $(".simLost_secondPopUP").show();
    });

    $("#simLostDone, #simLost_Cancel").on("click", function() {
        simRequest.close();
    });

    function resetsimRequestDialog() {
        //alert("sim close request");
        $(".simLost_FirstPopUP").show();
        $(".simLost_secondPopUP").hide();
        $(".validNumber").hide();
        $("dialog#simRequest #mobileNumber").val("");
        $("#simLost_confirm").addClass("mdl-button--disabled");
    }

    function validNumber() {
        if ($("#simRequest .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
            $("#simRequest .mobNumberInput #validationIcon").removeClass("invalidNumber");
        }
        if (!($("#simRequest .mobNumberInput #validationIcon").hasClass("validNumber"))) {
            $("#simRequest .mobNumberInput #validationIcon").addClass("validNumber");
        }
        if ($('#simRequest .mobNumberInput').hasClass("textfield__error")) {
            $('#simRequest .mobNumberInput').removeClass("textfield__error");
        }
        $("#simRequest #textfield__error_mobNo").css("display", "none");
        $("#simLost_confirm").removeClass("mdl-button--disabled");
    }

    function invalidNumberInput() {
        if ($("#simRequest .mobNumberInput #validationIcon").hasClass("validNumber")) {
            $("#simRequest.mobNumberInput #validationIcon").removeClass("validNumber");
        }
        if (!$("#simRequest .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
            $("#simRequest .mobNumberInput #validationIcon").addClass("invalidNumber");
        }
        if (!($('.simLost_FirstPopUP .mobNumberInput').hasClass("textfield__error"))) {
            $('.simLost_FirstPopUP .mobNumberInput').addClass("textfield__error");
        }

        $(".simLost_FirstPopUP #textfield__error_mobNo").css("display", "block");
        $("#simLost_confirm").addClass("mdl-button--disabled");
    }

    $("#simRequest #mobileNumber").on("input keyup", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        var reg = /^0+/gi;
        if (this.value.match(reg)) {
            this.value = this.value.replace(reg, '');
        }
        if (tempVal.length == 10 && (!isNaN(tempVal)) && event.type == 'input') {
            validNumber();
        } else if (tempVal.length > 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if (tempVal.length > 0 && tempVal.length < 10 && (event.keyCode == 8 || event.keyCode == 46)) {
            invalidNumberInput();
        } else {

            if (tempVal.length == 0) {
                $("#simLost_confirm").addClass("mdl-button--disabled");
                if ($('#simRequest .mobNumberInput #validationIcon').hasClass("validNumber")) {
                    $('#simRequest .mobNumberInput #validationIcon').removeClass("validNumber");
                }
                if ($('#simRequest .mobNumberInput #validationIcon').hasClass("invalidNumber")) {
                    $('#simRequest .mobNumberInput #validationIcon').removeClass("invalidNumber");
                }
                if ($('#simRequest .mobNumberInput').hasClass("textfield__error")) {
                    $('#simRequest .mobNumberInput').removeClass("textfield__error");
                }
            }
        }
    });



    /*Sim Block Request End*/



    /* Bill Not Received JS Start */
    $.fn.billNotReceivedDialog = function() {

        var billNotReceivedDialog = document.querySelector('dialog#billNotReceived-dialog');
        if (!billNotReceivedDialog.showModal) {
            dialogPolyfill.registerDialog(billNotReceivedDialog);
        }
        billNotReceivedDialog.showModal();
        billNotReceivedDialog.querySelector('dialog#billNotReceived-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            billNotReceivedDialog.close();
        });
        billNotReceivedDialog.querySelector('dialog#billNotReceived-dialog #billNotReceived_cancel').addEventListener('click', function(event) {
            event.preventDefault();
            billNotReceivedDialog.close();
        });

        /*  billNotReceivedDialog.addEventListener('click', function(event) {
        var rect = billNotReceivedDialog.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        if (!isInDialog) {
        billNotReceivedDialog.close();
        }
        }); */
    };

    /* Bill Not Received JS Start */
    /* Do Not Disturb Activation JS Start */
    $.fn.dndActivationDialog = function() {

        var dndActivationDialog = document.querySelector('dialog#dndActivation-dialog');
        if (!dndActivationDialog.showModal) {
            dialogPolyfill.registerDialog(dndActivationDialog);
        }
        resetDndRequestDialog();
        dndActivationDialog.showModal();
        dndActivationDialog.querySelector('dialog#dndActivation-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            dndActivationDialog.close();
        });
        dndActivationDialog.querySelector('dialog#dndActivation-dialog #dndActivation_cancel').addEventListener('click', function(event) {
            event.preventDefault();
            dndActivationDialog.close();
        });

        /*  dndActivationDialog.addEventListener('click', function(event) {
        var rect = dndActivationDialog.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        if (!isInDialog) {
        dndActivationDialog.close();
        }
        }); */

        $("#dndRequestDone").on("click", function() {
            dndActivationDialog.close();
        });

        function resetDndRequestDialog() {
            //alert("hello refersh");
            $(".dndActivation-dialog_firstPopUP").show();
            $(".dndActivation-dialog_secondPopUP").hide();
            $("dialog#dndActivation-dialog input:not(:disabled)").parent().removeClass("is-checked dndYes");

        }


        $("#blockAll").change(function() {
            alert("hello checkbox");
            if ($(this).parent().hasClass("is-checked")) {

                alert("checkbox is checked");

                $("#preCheckList label input:not(:disabled)").each(function() {
                    $(this).parent().addClass("is-checked dndYes");

                });
            } else {
                alert("remove checkbox is checked");

                $("#preCheckList label input:not(:disabled)").each(function() {

                    $(this).parent().removeClass("is-checked dndYes");

                });

            }
        });




    };





    $("#dndActivation_proceed").on("click", function() {

        if ($(".dndActivation_section input[type=checkbox]:not(:disabled):not('#tnc'):checked").length == 0) {

            alert("please select any one checkbox");

        } else if ($("#tnc:checked").length == 0) {
            alert("please check I have read and understood all the Terms & Conditions ")

        } else {
            $(".dndActivation-dialog_firstPopUP").hide();
            $(".dndActivation-dialog_secondPopUP").show();
        }
    });



    /* Do Not Disturb Activation JS Start */





    /* Complaints Bill Not Received JS Start */
    $.fn.cBillNotReceivedDialog = function() {

        var cBillNotReceivedDialog = document.querySelector('dialog#cBillNotReceived-dialog');
        if (!cBillNotReceivedDialog.showModal) {
            dialogPolyfill.registerDialog(cBillNotReceivedDialog);
        }
        cBillNotReceivedDialog.showModal();
        cBillNotReceivedDialog.querySelector('dialog#cBillNotReceived-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            cBillNotReceivedDialog.close();
        });
        cBillNotReceivedDialog.querySelector('dialog#cBillNotReceived-dialog #billNotReceived_cancel').addEventListener('click', function(event) {
            event.preventDefault();
            cBillNotReceivedDialog.close();
        });

        /*  billNotReceivedDialog.addEventListener('click', function(event) {
        var rect = billNotReceivedDialog.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        if (!isInDialog) {
        billNotReceivedDialog.close();
        }
        }); */
    };
    $("#billNotReceived_proceed").on("click", function() {
        $(".dialogMContainer").hide();
        $(".billNotReceived_section_secondPopUP").show();
    });

    $("#comDone").on("click", function() {
        alert("Hello");
        billNotReceivedDialog.close();
    });
    /* Complaints Bill Not Received JS End */



});

/* Manage Connection End */

/* Prepaid Manage Account JS Start */
$(document).ready(function() {
    /*#### Grid to list Start ####*/
    $('.pre_viewType').on("click", function() {
        var typView = $(this).attr('data-val');
        $("#pre_listGrid").removeClass().addClass(typView);
    });
    /*#### Grid to list End ####*/
    /* edit Account Start*/
    var pre_eNameDialog;
    var pre_allAcc_nickName;
    $('.pre_eNameLink').on("click", function() {
        var en = $(this).closest('#pre_listGrid .uniqueCardA_wrapper .cust_name').text();
        console.log(en);
        $.fn.pre_editNameDialog();

    });
    $.fn.pre_editNameDialog = function() {
        // alert('removed Called');
        $('dialog#pre_editName-dialog .dialogM_innnerWrap').find('.dialogMTitle').show();
        $('dialog#pre_editName-dialog .eName_firstBlock').show();
        $('dialog#pre_editName-dialog .eName_secondBlock').hide();
        $('dialog#pre_editName-dialog .eName_thirdBlock').hide();
        pre_eNameDialog = document.querySelector('dialog#pre_editName-dialog');
        if (!pre_eNameDialog.showModal) {
            dialogPolyfill.registerDialog(pre_eNameDialog);
        }
        pre_eNameDialog.showModal();
        pre_eNameDialog.querySelector('dialog#pre_editName-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            pre_eNameDialog.close();
        });


        pre_eNameDialog.addEventListener('click', function(event) {
            var rect = pre_eNameDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                pre_eNameDialog.close();
            }
        });

    };


    /* DialogM  Remove Account code End*/
    $("#pre_eNameProceed").on("click", function() {
        $('dialog#pre_editName-dialog .eName_firstBlock').hide();
        pre_allAcc_nickName = $('#pre_allAcc_nickName').val();
        $('dialog#pre_editName-dialog .eName_secondBlock').show();
        $('#pre_allAcc_nickNameNew').html(pre_allAcc_nickName);
    });

    $("#pre_eNameOTPProceed").on("click", function() {
        $(this).closest('.dialogM_innnerWrap').find('.dialogMTitle').hide();
        $('dialog#pre_editName-dialog .eName_thirdBlock').show();
        $('dialog#pre_editName-dialog .eName_secondBlock').hide();
    });

    $("#pre_editNameDone, #pre_eNameCancel1, #pre_eNameCancel2").on("click", function() {
        pre_eNameDialog.close();
    });

    /* edit Account End*/
    /* DialogM  Remove Account code Start*/
    var pre_removeDialog;
    var user;
    var dcount;
    $('.pre_removeAccountLink').on("click", function() {
        user = $(this).attr("data-user");
        dcount = $(this).attr("data-count");
        $.fn.pre_removeAccountDialog(user);

    });
    $.fn.pre_removeAccountDialog = function(user) {
        console.log(user);
        // alert('removed Called');
        $('.rName').text(user);
        $('dialog#pre_removeAccount-dialog .removeAccount_FirstPopUP').show();
        $('dialog#pre_removeAccount-dialog .removeAccount_SecondPopUP').hide();
        pre_removeDialog = document.querySelector('dialog#pre_removeAccount-dialog');
        if (!pre_removeDialog.showModal) {
            dialogPolyfill.registerDialog(pre_removeDialog);
        }

        pre_removeDialog.showModal();
        pre_removeDialog.querySelector('dialog#pre_removeAccount-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            pre_removeDialog.close();
        });


        pre_removeDialog.addEventListener('click', function(event) {
            var rect = pre_removeDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                pre_removeDialog.close();
            }
        });

    };


    /* DialogM  Remove Account code End*/
    $("#pre_removeAccount_confirm").on("click", function() {
        console.log("##" + dcount);
        $(this).closest('.dialogM_innnerWrap').find('.dialogMTitle').hide();
        //$('.dialogMTitle').hide();
        $('dialog#pre_removeAccount-dialog .removeAccount_FirstPopUP').hide();
        $('dialog#pre_removeAccount-dialog .removeAccount_SecondPopUP').show();
    });

    $("#pre_removeAccount_Done, #pre_removeAccount_Cancel").on("click", function() {

        pre_removeDialog.close();
    });

    /* Add Account DialogM Code Start*/
    var pre_addAccountDialog;
    $('.pre_addAccount').on("click", function() {
        $.fn.pre_myAccountDialog();
    });

    $.fn.pre_myAccountDialog = function() {
        $('dialog#pre_addAccount-dialog .add_acc_dialog_first').show();
        $('dialog#pre_addAccount-dialog .dialogM_innnerWrap').find('.dialogMTitle').show();
        $('dialog#pre_addAccount-dialog .add_acc_dialog_second').hide();
        $('dialog#pre_addAccount-dialog .add_acc_dialog_third').hide();
        pre_addAccountDialog = document.querySelector('dialog#pre_addAccount-dialog');
        if (!pre_addAccountDialog.showModal) {
            dialogPolyfill.registerDialog(pre_addAccountDialog);
        }

        pre_addAccountDialog.showModal();
        pre_addAccountDialog.querySelector('dialog#pre_addAccount-dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            pre_addAccountDialog.close();
        });


        pre_addAccountDialog.addEventListener('click', function(event) {
            var rect = pre_addAccountDialog.getBoundingClientRect();
            var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog) {
                pre_addAccountDialog.close();
            }
        });

    };


    $("#pre_add_acc_proceed1").on("click", function() {
        $('dialog#pre_addAccount-dialog .add_acc_dialog_first').hide();
        $('dialog#pre_addAccount-dialog .add_acc_dialog_second').show();
        $('dialog#pre_addAccount-dialog .add_acc_info').show();

    });
    $("#pre_add_acc_proceed2").on("click", function() {
        $(this).closest('.dialogM_innnerWrap').find('.dialogMTitle').hide();
        $('dialog#pre_addAccount-dialog .add_acc_dialog_third').show();
        $('dialog#pre_addAccount-dialog .add_acc_dialog_second').hide();

    });
    $("#pre_addAccountDialog_Done").on("click", function() {

        pre_addAccountDialog.close();
        $(".pre_AddUser").clone().removeClass('addUser').appendTo("#pre_listGrid");
    });
    $("#pre_add_acc_cancel1, #pre_add_acc_cancel2").on("click", function() {
        pre_addAccountDialog.close();
    });
    /* DialogM  Edit name code End */


});
/* Prepaid Manage Account JS End */



/* post-my-complaints Start  */

function show(aval) {
    if (aval == "Bill Not Received") {

        $("#myRequstChanges").on("click", function() {

            window.location = "complaints_bill-not-recieved.html";
        });
    } else if (aval == "Coverage") {

        $("#myRequstChanges").on("click", function() {

            window.location = "complaints_coverage.html";
        });
    } else if (aval == "Net Not Working") {

        $("#myRequstChanges").on("click", function() {

            window.location = "complaints_netNotWorking.html";
        });
    } else if (aval == "web related") {

        $("#myRequstChanges").on("click", function() {

            window.location = "complaints_webRelated.html";
        });
    }
}
/* post-my-complaints  End  */
/*--------------------Prepaid - My service Popup code----------------------------- */
/* international Roaming code Start*/
var preRoamingDialog;
$('input#pre-roaming-switch').on("change", function() {
    $.fn.preinternationalRoamingDialog();
    if ($('#pre-roaming-text').is('.is-checked') === false) {

        $("div#pre-roaming-switch-label").text("ON");
    } else {

        $("div#pre-roaming-switch-label").text("OFF");
    }
});

$.fn.preinternationalRoamingDialog = function() {
    $('.internationalRoaming_secondPopup').hide();
    $('.internationalRoaming_firstPopup').show();
    preRoamingDialog = document.querySelector('dialog#pre-internationalRoaming-dialog');
    if (!preRoamingDialog.showModal) {
        dialogPolyfill.registerDialog(preRoamingDialog);
    }
    preRoamingDialog.showModal();
    preRoamingDialog.querySelector('dialog#pre-internationalRoaming-dialog .close').addEventListener('click', function(event) {
        event.preventDefault();
        preRoamingDialog.close();
    });

    $("dialog#pre-internationalRoaming-dialog").on("click", function(event) {

        var rect = this.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        console.log(rect.height + "width" + rect.width);


        if (!isInDialog) {
            preRoamingDialog.close();
        } else {
            preRoamingDialog = document.querySelector('dialog#pre-internationalRoaming-dialog');
            if (!preRoamingDialog.showModal) {

                preRoamingDialog.close();
            }
        }
    });
};

$("#pre-internationalRoaming_confirm").on("click", function() {

    $('.internationalRoaming_firstPopup').hide();
    $('.internationalRoaming_secondPopup').show();
    $("p#pre-roaming-msg").css("display", "block");
    $('#pre-roaming-switch').attr('disabled', 'disabled');

    setInterval(function() {
        $('#pre-roaming-switch').prop('disabled', false);
        $("p#pre-roaming-msg").css("display", "none");

    }, 30000);
});

$("#pre-internationalRoaming_done, #pre-internationalRoaming_cancel").on("click", function() {

    if (preRoamingDialog.showModal) {
        preRoamingDialog.close();
    }
});

/* international Roaming code End*/

/* isd code Start*/
var preIsdDialog;
$('input#pre-isd-switch').change("click", function() {
    $.fn.preInternationalISDDialog();
    if ($('#pre-isd-text').is('.is-checked') === false) {
        $("div#pre-isd-switch-label").text("ON");
    } else {
        $("div#pre-isd-switch-label").text("OFF");
    }
});

$.fn.preInternationalISDDialog = function() {
    $('.isd_secondPopup').hide();
    $('.isd_firstPopup').show();
    preIsdDialog = document.querySelector('dialog#pre-internationalISD-dialog');
    if (!preIsdDialog.showModal) {
        dialogPolyfill.registerDialog(preIsdDialog);
    }
    preIsdDialog.showModal();
    preIsdDialog.querySelector('dialog#pre-internationalISD-dialog .close').addEventListener('click', function(event) {
        event.preventDefault();
        preIsdDialog.close();
    });

    $("dialog#pre-internationalISD-dialog").on("click", function(event) {

        var rect = this.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        console.log(rect.height + "width" + rect.width);


        if (!preIsInDialog) {
            preIsdDialog.close();
        } else {
            preIsdDialog = document.querySelector('dialog#pre-internationalISD-dialog');
            if (!preIsdDialog.showModal) {

                preIsdDialog.close();
            }
        }
    });
};

$("#pre-isd_confirm").on("click", function() {
    $('.isd_firstPopup').hide();
    $('.isd_secondPopup').show();
    $("p#pre-isd-msg").css("display", "block");
    $('#pre-isd-switch').attr('disabled', 'disabled');
    setInterval(function() {
        $('#pre-isd-switch').prop('disabled', false);
        $("p#pre-isd-msg").css("display", "none");
    }, 30000);
});

$("#pre-isd_done, #pre-isd_cancel").on("click", function() {

    if (preIsdDialog.showModal) {
        preIsdDialog.close();
    }
});

/* isd code End*/

/* Call Conference code Start*/
var preCCDialog;
$('input#pre-cc-switch').change("click", function() {
    $.fn.preInternationalCCDialog();
    if ($('#pre-cc-text').is('.is-checked') === false) {
        $("div#pre-cc-switch-label").text("ON");
    } else {
        $("div#pre-cc-switch-label").text("OFF");
    }
});

$.fn.preInternationalCCDialog = function() {
    $('.cc_secondPopup').hide();
    $('.cc_firstPopup').show();
    preCCDialog = document.querySelector('dialog#pre-internationalCC-dialog');
    if (!preCCDialog.showModal) {
        dialogPolyfill.registerDialog(preCCDialog);
    }
    preCCDialog.showModal();
    preCCDialog.querySelector('dialog#pre-internationalCC-dialog .close').addEventListener('click', function(event) {
        event.preventDefault();
        preCCDialog.close();
    });

    $("dialog#internationalCC-dialog").on("click", function(event) {

        var rect = this.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        console.log(rect.height + "width" + rect.width);


        if (!isInDialog) {
            preCCDialog.close();
        } else {
            preCCDialog = document.querySelector('dialog#pre-internationalCC-dialog');
            if (!preCCDialog.showModal) {

                preCCDialog.close();
            }
        }
    });
};

$("#pre-cc_confirm").on("click", function() {
    $('.cc_firstPopup').hide();
    $('.cc_secondPopup').show();
    $("p#pre-cc-msg").css("display", "block");
    $('#pre-cc-switch').attr('disabled', 'disabled');

    setInterval(function() {
        $('#pre-cc-switch').prop('disabled', false);
        $("p#pre-cc-msg").css("display", "none");
    }, 30000);
});

$("#pre-cc_done, #pre-cc_cancel").on("click", function() {

    if (preCCDialog.showModal) {
        preCCDialog.close();
    }
});

/* Call Conference code End*/

/* Missed Call Alert code Start*/
var preMCADialog;
$('input#pre-mca-switch').change("click", function() {
    $.fn.preInternationalMCADialog();
    if ($('#pre-mca-text').is('.is-checked') === false) {
        $("div#pre-mca-switch-label").text("ON");
    } else {
        $("div#pre-mca-switch-label").text("OFF");
    }
});

$.fn.preInternationalMCADialog = function() {
    $('.mca_secondPopup').hide();
    $('.mca_firstPopup').show();
    preMCADialog = document.querySelector('dialog#pre-internationalMCA-dialog');
    if (!preMCADialog.showModal) {
        dialogPolyfill.registerDialog(preMCADialog);
    }
    preMCADialog.showModal();
    preMCADialog.querySelector('dialog#pre-internationalMCA-dialog .close').addEventListener('click', function(event) {
        event.preventDefault();
        preMCADialog.close();
    });

    $("dialog#pre-internationalMCA-dialog").on("click", function(event) {

        var rect = this.getBoundingClientRect();
        var isInDialog = (rect.top <= event.clientY && event.clientY <= rect.top + rect.height && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
        console.log(rect.height + "width" + rect.width);


        if (!isInDialog) {
            preMCADialog.close();
        } else {
            preMCADialog = document.querySelector('dialog#pre-internationalMCA-dialog');
            if (!preMCADialog.showModal) {

                preMCADialog.close();
            }
        }
    });
};

$("#pre-mca_confirm").on("click", function() {

    $('.mca_firstPopup').hide();
    $('.mca_secondPopup').show();
    $("p#pre-mca-msg").css("display", "block");

    $('#pre-mca-switch').attr('disabled', 'disabled');

    setInterval(function() {
        $('#pre-mca-switch').prop('disabled', false);
        $("p#pre-mca-msg").css("display", "none");
    }, 30000);
});

$("#pre-mca_done, #pre-mca_cancel").on("click", function() {

    if (preMCADialog.showModal) {
        preMCADialog.close();
    }
});

/* Missed Call Alert code End*/



/* Prepaid My request Pages Start */

/* Update GSTIN-dialog Request Start*/
$(document).ready(function() {
    $.fn.updateGSTINDialogs = function() {

        setTimeout(function() {
            console.log("updateGSTINDialogs");
            $("#gstinNumber").focus();
        }, 100);


        var updateGSTINDialog = document.querySelector('dialog#updateGSTIN_dialog');


        if (!updateGSTINDialog.showModal) {
            dialogPolyfill.registerDialog(updateGSTINDialog);
        }
        resetsimRequestDialog();
        updateGSTINDialog.showModal();
        updateGSTINDialog.querySelector('dialog#updateGSTIN_dialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            updateGSTINDialog.close();
        });

        $('dialog#updateGSTIN_dialog #gstinDone, dialog#updateGSTIN_dialog #gstinCancel').click(function(event) {
            //alert("Gstin Testing");
            event.preventDefault();
            resetsimRequestDialog();
            updateGSTINDialog.close();
        });

    };





    $(".gstinPack_firstPopup #gstinPack_confirm").on("click", function() {
        $('.gstinPack_firstPopup').hide();
        $('.gstinPack_secondPopup').show();

    });

    $("#updateGSTIN_dialog #gstinNumber").on("keyup input", function(event) {
        var inputvalues = $(".gstinPack_firstPopup #gstinNumber").val();
        var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');

        if (this.value.length == 15) {
            if (gstinformat.test(inputvalues) == false) {
                $("#textfield__error_gstNo").show();
                $(".gstNumberInput").addClass("textfield__error");
                $("#gstinPack_confirm").addClass("mdl-button--disabled");
            } else {
                $("#textfield__error_gstNo").hide();
                $(".gstNumberInput").removeClass("textfield__error");
                $("#gstinPack_confirm").removeClass("mdl-button--disabled");
            }
        } else if (this.value.length < 15 && this.value.length > 0 && (event.keyCode == 46 || event.keyCode == 8)) {
            $("#textfield__error_gstNo").show();
            $(".gstNumberInput").addClass("textfield__error");
            $("#gstinPack_confirm").addClass("mdl-button--disabled");
        } else if (this.value.length == 0) {
            $("#textfield__error_gstNo").hide();
            $(".gstNumberInput").removeClass("textfield__error");
            $("#gstinPack_confirm").addClass("mdl-button--disabled");
        }
    });



    function resetsimRequestDialog() {
        //alert(resetsimRequestDialog);
        $(".gstinPack_firstPopup").show();
        $(".gstinPack_secondPopup").hide();
        $("dialog#updateGSTIN_dialog #gstinNumber").val("");
        $("#textfield__error_gstNo").hide();
        $("#gstinPack_confirm").addClass("mdl-button--disabled");

    }


    /* Update GSTIN-dialog Request End*/

    /*Prepaid statement on mail start*/

    $.fn.prepaidStatementDialog = function() {

        var prepaidStatementM = document.querySelector('dialog#prepaidStatement');
        if (!prepaidStatementM.showModal) {
            dialogPolyfill.registerDialog(prepaidStatementM);
        }
        resetDialog();
        prepaidStatementM.showModal();
        prepaidStatementM.querySelector('dialog#prepaidStatement .close').addEventListener('click', function(event) {
            event.preventDefault();
            prepaidStatementM.close();
        });


        $('dialog#prepaidStatement #preStCancel, dialog#prepaidStatement #preStDone').click(function(event) {
            event.preventDefault();
            location.reload();
            prepaidStatementM.close();
            console.log("preStateM is called");


        });


    };


    $("#preStCancel").on("click", function() {
        $('#preStateM option:first').prop('selected', true);
        $('.preState_firstPopup').hide();
        $('.preState_secondPopup').show();

    });
    $("#preStateconfirm").on("click", function() {
        alert($("#modeofDel").text());
        $('#preStateM option:first').prop('selected', true);
        if ($("#modeofDel").text() != "") {
            $('.preState_firstPopup').hide();
            $('.preState_secondPopup').show();
        } else {
            $('.preState_firstPopup').hide();
            $('.preState_errorMsg').show();
        }
    });

    function resetDialog() {
        // alert("resetDialog");
        $(".preState_firstPopup").show();
        $(".preState_secondPopup").hide();
        $(".preState_errorMsg").hide();
        $("#preStateconfirm").val("").addClass("mdl-button--disabled");
    }

    $("#preStateM").change(function() {
        console.log($("#modeofDel").text());
        if (!($(this).val() == 0)) {
            $("#preStateconfirm").removeClass("mdl-button--disabled");
        } else if (!$("#preStateconfirm").hasClass("mdl-button--disabled")) {
            $("#preStateconfirm").addClass("mdl-button--disabled");
        }
    });


    /*Prepaid statement on mail end*/




    /* DND Deactivation Dialog Start */

    $.fn.DeactivationDialogs = function() {

        var ddeactivationDIalog = document.querySelector('dialog#DeactivationDialog');
        if (!ddeactivationDIalog.showModal) {
            dialogPolyfill.registerDialog(DeactivationDialog);
        }
        resetDeactivationDialog();
        ddeactivationDIalog.showModal();
        ddeactivationDIalog.querySelector('dialog#DeactivationDialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            ddeactivationDIalog.close();
        });


        $('dialog#DeactivationDialog #ddndActivation_cancel, dialog#DeactivationDialog #ddndRequestDone').click(function(event) {
            event.preventDefault();
            ddeactivationDIalog.close();
        });


        $("#dblockAll").change(function() {
            alert("hello checkbox");
            if ($(this).parent().hasClass("is-checked")) {
                alert("checkbox is checked");
                $("#preCheckList label input:not(:disabled)").each(function() {
                    $(this).parent().addClass("is-checked dndYes");

                });
            } else {
                alert("remove checkbox is checked");

                $("#preCheckList label input:not(:disabled)").each(function() {

                    $(this).parent().removeClass("is-checked dndYes");

                });

            }
        });
        $("#dndRequestDone").on("click", function() {
            ddeactivationDIalog.close();
        });

    };


    /* DND Deactivation Dialog End */



    function resetDeactivationDialog() {
        //alert("hello refersh");
        $(".dndActivation-dialog_firstPopUP").show();
        $(".dndActivation-dialog_secondPopUP").hide();
        $("dialog#DeactivationDialog input:not(:disabled)").parent().removeClass("is-checked dndYes");

    }


    $("#ddndActivation_proceed").on("click", function() {

        if ($(".dndActivation_section input[type=checkbox]:not(:disabled):not('#dtnc'):checked").length == 0) {

            alert("please select any one checkbox");

        } else if ($("#dtnc:checked").length == 0) {
            alert("please check I have read and understood all the Terms & Conditions ")

        } else {
            $(".dndActivation-dialog_firstPopUP").hide();
            $(".dndActivation-dialog_secondPopUP").show();
        }
    });

});



/* prpaid-my-complaints Start  */

function shows(aval) {

    if (aval == "Recharge benefits not received") {
        $("#myRequstChanges").on("click", function() {

            window.location = "pre_recharge_benefits_not_received.html";
        });
    } else if (aval == "Coverage") {
        $("#myRequstChanges").on("click", function() {

            window.location = "pre_complaints_coverage.html";
        });
    } else if (aval == "GPRS/MMS not working") {
        $("#myRequstChanges").on("click", function() {
            alert("pre_complaints_netNotWorking");
            window.location = "pre_complaints_netNotWorking.html";
        });
    } else if (aval == "web related") {
        $("#myRequstChanges").on("click", function() {
            alert("pre_complaints_webRelated");
            window.location = "pre_complaints_webRelated.html";
        });
    }
}

/* prepaid-my-complaints  End  */
/* Prepaid My request Pages end */
/* Bulk bill pay and dropdown Start */
$(document).ready(function() {
    var $qqq;

    $('.bbpIcon').on("click", function() {
        // alert('####');
        $(this).toggleClass('open');
        $('.custHeadlineD').toggle();
        $('.billBlockWrapper').toggle();
    });
    $('.dMenuBlock').on("click", function(e) {
        $('.dMenuCon').hide();

        //$('.dMenuCon').removeClass('hide');
        // $(this).parent('.uniqueCardA_block4').css('background', "red");
        $qqq = $(this).closest('.custMenu').find('.dMenuCon');
        $(this).closest('.custMenu').find('.dMenuCon').removeClass('hide').slideDown(200);
        //$(this).closest('div').hasAttr($(this).attr('id')).removeClass('hide');
        e.stopPropagation();


    });
    $(document.body).click(function(e) {
        $count = 0;
        if ($qqq != undefined) {
            if (!($qqq).hasClass('hide')) {
                console.log('bodyclick' + $qqq);
                $qqq.slideUp(200);
            }
        }
    });

    $("#prebnRcancel").click(function(event) {
        window.location = "pre-my-request.html";

    });
    /* Bulk bill pay and dropdown End */

});






/* Prepaid Complaints Coverage Start */

$(document).ready(function() {
    $("#mgmLeadDialog #mobileNumber").on("keyup input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 10 && (!isNaN(tempVal)) && (event.type = "input")) {
            $(this).closest('.mobNumberInput').find('.mdl-textfield__error_msg').hide();
            //validNumber();
        } else if (tempVal.length > 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if ((this.value.length > 0 && this.value.length < 10) && (event.keyCode == 8 || event.keyCode == 46)) {
            //invalidNumber();
            $(this).closest('.mobNumberInput').find('.mdl-textfield__error_msg').show();
        } else {
            // invalidNumber();
            $(this).closest('.mobNumberInput').find(".mdl-textfield__error_msg").css("display", "none");
            if ($(".mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $(".mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            if ($(".mobNumberInput #validationIcon").hasClass("validNumber")) {
                $(".mobNumberInput #validationIcon").removeClass("validNumber");
            }
        }
    });


    $("#mgmLeadDialog #rContactNumber").on("keyup input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 10 && (!isNaN(tempVal)) && (event.type = "input")) {
            $(this).closest('.cmobNumberInput').find('.mdl-textfield__error_msg').hide();
            //validNumber();
        } else if (tempVal.length > 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if ((this.value.length > 0 && this.value.length < 10) && (event.keyCode == 8 || event.keyCode == 46)) {
            //invalidNumber();
            $(this).closest('.cmobNumberInput').find('.mdl-textfield__error_msg').show();
        } else {
            // invalidNumber();
            $(this).closest('.cmobNumberInput').find(".mdl-textfield__error_msg").css("display", "none");
            if ($(".cmobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $(".cmobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            if ($(".cmobNumberInput #validationIcon").hasClass("validNumber")) {
                $(".cmobNumberInput #validationIcon").removeClass("validNumber");
            }
        }
    });



    $("#buildingName,#areaField,#landmarkCity,#pincodeCity").on("keyup input", function(event) {
        if (($(this).val().length >= 0)) {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");
        }
    });
    $('#mgmLeadconfirm').on("click", function() {
        var mgmReturn = true;
        rValidNumber();
        rBuildName();
        rValidArea();
        rValidLandmark();
        rValidPc();
        rValidCNumber();
        if (mgmReturn === true) {

        } else {
            console.log("invalid");
            return false;
        }
        console.log("valid");
        $('.mgmLead_secondPopup').show();
        $('.mgmLead_firstPopup').hide();
        // Valid Mobile Number 
        function rValidNumber() {
            // alert("rValidNumber");
            if ($('#mobileNumber').val().length == 10) {
                $("#mobileNumber").siblings(".mdl-textfield__error_msg").hide();
                //  $("#mobileNumber").siblings("#validationIcon").addClass("invalidNumber");
                //  $("#mobileNumber").siblings(".mdl-textfield__error_msg")..css("display", "block");

            } else {
                // alert("Valid Mobile Number");
                $("#mobileNumber").siblings('.mdl-textfield__error_msg').css("display", "block");
                //$('.mdl-textfield__error_msg').css("display", "block");
                $('#mobileNumber').parent().addClass("textfield__error");
                $('#mobileNumber').focus();
                mgmReturn = false;

            }

            // alert("rValidNumber " + mgmReturn);
        }




        // Valid Building Name 
        function rBuildName() {
            if ($('#buildingName').val().length == 0) {
                $("#buildingName").siblings('.mdl-textfield__error_msg').css("display", "block");
                mgmReturn = false;
            } else {
                //  alert("Valid Building Name");
                $("#buildingName").siblings(".mdl-textfield__error_msg").css("display", "none");

            }
            // alert("rBuildName " + mgmReturn);
        }

        // Valid Area Name 
        function rValidArea() {
            // alert("rValidArea");
            if ($('#areaField').val().length == 0) {
                // alert("Inidai is calling");
                $("#areaField").siblings('.mdl-textfield__error_msg').css("display", "block");
                mgmReturn = false;
            } else {
                //alert(" Valid Area Name");
                $("#areaField").siblings(".mdl-textfield__error_msg").css("display", "none");

            }

            // alert("rValidArea " + mgmReturn);
        }


        // Valid Landmark Name 
        function rValidLandmark() {
            // alert("rValidLandmark");
            if ($('#landmarkCity').val().length == 0) {
                $("#landmarkCity").siblings('.mdl-textfield__error_msg').css("display", "block");
                mgmReturn = false;
            } else {
                $("#landmarkCity").siblings(".mdl-textfield__error_msg").css("display", "none");

            }

            // alert("rValidLandmark " + mgmReturn);
        }

        // Valid Pincode  
        function rValidPc() {
            // alert("pincode");
            if ($('#pincodeCity').val().length != 6) {
                $("#pincodeCity").siblings('.mdl-textfield__error_msg').css("display", "block");
                mgmReturn = false;
            } else {
                $("#pincodeCity").siblings(".mdl-textfield__error_msg").css("display", "none");


            }
            // alert("rValidPc " + mgmReturn);
        }

        // Valid Contact Number
        function rValidCNumber() {
            //  alert("rValidNumber");
            if ($('#rContactNumber').val().length == 10) {
                $("#rContactNumber").siblings("#textfield__error_mobileNo").hide();
                $('#rContactNumber').parent().removeClass("textfield__error");


            } else {
                //  $('#rContactNumber').siblings(".mdl-textfield__error").show();
                $('#rContactNumber').parent().addClass("textfield__error");
                $("#rContactNumber").siblings(".mdl-textfield__error_msg").css("display", "block");
                mgmReturn = false;
            }
            // alert("rValidCNumber " + mgmReturn);
        }


    });


    // MGM Radio Button 
    $('#mgMLeadRequest .mdl-radio__button').on("change", function() {
        console.log("Radio button is MGM Request is called");
        mgmResetLead();

    });


    function mgmResetLead() {
        console.log("Hello MGM Lead Pages");
        $("#mobileNumber").val('');
        $(".mdl-textfield__error_msg").hide();
        $("#validationIcon").removeClass("invalidNumber validNumber");
        $("#buildingName").val('');
        $("#areaField").val('');
        $("#pincodeCity").val('');
        $("#landmarkCity").val('');
        $("#rContactNumber").val('');
        $("#mgMLeadRequest .mdl-textfield").removeClass("is-dirty");
    }




    /* MGM lead- WF awaited start */
    $.fn.mgmLeadDialogs = function() {

        setTimeout(function() {
            console.log("mgmLeadDialogs");
            $("#mgMLeadRequest #mobileNumber").focus();
        }, 100);


        var mgmLeadDIalog = document.querySelector('dialog#mgmLeadDialog');
        if (!mgmLeadDIalog.showModal) {
            dialogPolyfill.registerDialog(mgmLeadDIalog);
        }
        resetmgmDialog();
        mgmLeadDIalog.showModal();
        mgmLeadDIalog.querySelector('dialog#mgmLeadDialog .close').addEventListener('click', function(event) {
            event.preventDefault();
            mgmLeadDIalog.close();
        });


        $('dialog#mgmLeadDialog #mgmLeadCancel, dialog#mgmLeadDialog #mgmLeadDone').click(function(event) {
            event.preventDefault();
            mgmLeadDIalog.close();
        });


    };

    $("#pincodeCity").on("input", function(event) {

        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 6 && (!isNaN(tempVal))) {
            // validNumber();
        } else if (tempVal.length > 6) {
            $(this).val($(this).val().substring(0, 6) + '');
        } else if (tempVal.length > 0 && tempVal.length < 6 && (event.keyCode == 8 || event.keyCode == 46)) {
            //invalidNumber();
        } else {

        }

    });


    // $("#mgmLeadconfirm").on("click", function() {
    //     $('.mgmLead_firstPopup').hide();
    //     $('.mgmLead_secondPopup').show();

    // });

    function resetmgmDialog() {
        // alert("resetMGDialog");
        mgmResetLead();
        $(".mgmLead_firstPopup").show();
        $(".mgmLead_secondPopup").hide();
    }


    $('#mgMLeadRequest #mobileNumber').on("keydown", function(e) {
        console.log("Mobile is running is running");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }
        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }


    });


    $('#mgMLeadRequest #buildingName').on("keydown", function(e) {
        console.log("buildingName is running ");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }


    });



    $('#mgMLeadRequest #areaField').on("keydown", function(e) {
        console.log("Mobile is running is running");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }

    });



    $('#mgMLeadRequest #landmarkCity').on("keydown", function(e) {
        console.log("landmarkCity is running");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }
        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }

    });


    $('#mgMLeadRequest #pincodeCity').on("keydown", function(e) {
        console.log("pincodeCity is running");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }
        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }
    });


    $('#mgMLeadRequest #rContactNumber').on("keydown", function(e) {
        console.log("Alternat ContactNumber is running");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }
        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }

    });



    /* MGM lead- WF awaited end */



    /* Prepaid Complaints Coverage End */


    /* Do Not Disturb Activation Already Activaed Dialog Start */

    $.fn.dndSuccessDialog = function() {

        var dndSuccesDialog = document.querySelector('dialog#dndActivationDialogSucess');
        if (!dndSuccesDialog.showModal) {
            dialogPolyfill.registerDialog(dndSuccesDialog);
        }
        dndSuccesDialog.showModal();
        dndSuccesDialog.querySelector('dialog#dndActivationDialogSucess .close').addEventListener('click', function(event) {
            event.preventDefault();
            dndSuccesDialog.close();
        });

        $('dialog#dndSuccessDialog #preCov_cancel').click(function(event) {
            event.preventDefault();
            dndSuccesDialog.close();
        });
    };
    $("#dndSuccesDone").click(function(event) {
        window.location = "pre-my-request.html";
    });

    /* Do Not Disturb Activation Already Activaed Dialog Start */

    /* Prepaid Coverage Page Start */

    $("#duPro,#handsetMN,#exactAddress,#exactArea,#preCity,#covComments").on("keyup input", function(event) {
        if (($(this).val().length >= 0)) {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");
        } else {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");
        }


    });

    $("#covPro,#signalBars").on("change", function(event) {
        if (($(this).val().length == 0) && ($(this).val() == "")) {
            console.log("value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");

        } else {
            console.log(" Else value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");

        }
    });




    $('#comCoverageForm .mdl-radio__button').on("change", function() {
        console.log("Radio button is prepaid coverage is called");
        preResetCoverage();

    });


    function preResetCoverage() {
        $("#duPro").val('');
        $(".mdl-textfield__error_msg").hide();
        $('#covPro option:first').prop('selected', true);
        $('#signalBars option:first').prop('selected', true);
        $("#handsetMN").val('');
        $("#exactAddress").val('');
        $("#preCity").val('');
        $("#exactArea").val('');
        $("#covComments").val('');
        $("#comCoverageForm .mdl-textfield").removeClass("is-dirty");
    }

    $('#pre_coverage').on("click", function() {
        console.log("Prepaid Coverage is called");
        var preReturn = true;
        coverageProblem();
        numberSignalVisble();
        durationofProblem();
        handsetModelNumber();
        exactAdressLoc();
        coverageCity();
        coverageArea();
        coverageComments();
        if (preReturn === true) {
            console.log("Valid is called");

            alert("form is sbumiited Valid is called");
        } else {
            alert("Not is form is sbumiited Valid is called");
            console.log("invalid");
            return false;
        }



        // Where is the Coverage Problem
        function coverageProblem() {
            if (($("#covPro").val().length == 0) && ($("#covPro").val() == "")) {
                console.log("value is called");
                $("#covPro").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                console.log(" Else value is called");
                $("#covPro").siblings('.mdl-textfield__error_msg').css("display", "none");
            }
            alert("coverageProblem" + preReturn);
        }

        // Number of signal bars visible 
        function numberSignalVisble() {
            if (($("#signalBars").val().length == 0) && ($("#signalBars").val() == "")) {
                console.log("value is called");
                $("#signalBars").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                console.log(" Else value is called");
                $("#signalBars").siblings('.mdl-textfield__error_msg').css("display", "none");
            }
            alert("numberSignalVisble" + preReturn);
        }



        // Duration Of problem (e.g Days,Weeks,Months)
        function durationofProblem() {
            if ($('#duPro').val().length == 0) {
                $("#duPro").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                $("#duPro").siblings('.mdl-textfield__error_msg').css("display", "none");
            }
            alert("durationofProblem" + preReturn);
        }


        // Handset Model & Number Start
        function handsetModelNumber() {
            if ($('#handsetMN').val().length == 0) {
                $("#handsetMN").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                $("#handsetMN").siblings(".mdl-textfield__error_msg").css("display", "none");
            }

            alert("handsetModelNumber   " + preReturn);
        }

        // Exact address location Start
        function exactAdressLoc() {
            if ($('#exactAddress').val().length == 0) {
                $("#exactAddress").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                $("#exactAddress").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            alert("exactAdressLoc   " + preReturn);
        }


        // Coverage City Start
        function coverageCity() {
            if ($('#preCity').val().length == 0) {
                $("#preCity").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                $("#preCity").siblings(".mdl-textfield__error_msg").css("display", "none");
            }

            alert("coverageCity   " + preReturn);
        }


        // Coverage Area Start
        function coverageArea() {
            if ($('#exactArea').val().length == 0) {
                $("#exactArea").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                $("#exactArea").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            alert("coverageArea   " + preReturn);
        }



        // Coverage Area Start
        function coverageComments() {
            if ($('#covComments').val().length == 0) {
                $("#covComments").siblings('.mdl-textfield__error_msg').css("display", "block");
                preReturn = false;
            } else {
                $("#covComments").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            alert("coverageComments   " + preReturn);
        }

    });




    $("#covComments").on('keyup', function() {
        console.log("Comments Called");
        var words = this.value.length;
        if (words > 250) {
            var trimmed = $(this).val();
            $(this).val(trimmed + " ");
        } else {
            $('.coutNum').text(250 - words);
        }
    });

    $('#comCoverageForm #covPro').on("keydown", function(e) {
        console.log("Coverage Problem is selected done");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val().length == 0) {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }
    });


    $('#comCoverageForm #signalrec').on("keydown", function(e) {
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val().length == 0) {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }
    });



    $('#comCoverageForm #handsetMN').on("keydown", function(e) {
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }
    });

    $('#comCoverageForm #exactAddress').on("keydown", function(e) {
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }

    });

    $('#comCoverageForm #preCity').on("keydown", function(e) {
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }


    });

    $('#comCoverageForm #exactArea').on("keydown", function(e) {
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }

    });


    $('#comCoverageForm #covComments').on("input keydown", function(e) {
        console.log("CovComments is running");
        var code = e.keyCode || e.which;
        $(this).css('border', '');
        //$(this).next().next().html('');
        $(this).closest('div').find('span.mdl-textfield__error_msg').show();
        if (code == '9') {
            console.log("comments is running");
            if ($(this).val() == '') {
                $(this).closest('div').find('span.mdl-textfield__error_msg').show();
                $(this).focus();
                return;
            } else {
                console.log("3 comments is running");
                $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
            }

        } else {
            $(this).closest('div').find('span.mdl-textfield__error_msg').hide();
        }

    });



    /* Prepaid Coverage Page End */




    /* Prepaid Make a Complaint | Web Related  Start*/


    $("#sample5,#faceCon").on("input keyup keydown", function(event) {
        if (($(this).val().length >= 0)) {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");
        } else {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");
        }

    });

    $("#typeCWeb").on("change", function(event) {
        if (($(this).val().length == 0) && ($(this).val() == "")) {
            console.log("value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");

        } else {
            console.log(" Else value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");

        }
    });



    $("#PreWebRConfirm").on("click", function() {
        console.log("Make a Complaint | Web Related is called");
        var webReturn = true;
        complaintsWR();
        faceConcern();
        webComments();

        if (webReturn === true) {
            console.log("Valid is called");
            alert("Your form is sbumiited Successfully");
        } else {
            alert("form is Not sbumiited ");
            console.log("invalid");
            return false;
        }

        // Type of Concern web related
        function complaintsWR() {
            if (($("#typeCWeb").val().length == 0) && ($("#typeCWeb").val() == "")) {
                console.log("value is called");
                $("#typeCWeb").siblings('.mdl-textfield__error_msg').css("display", "block");
                webReturn = false;
            } else {
                console.log(" Else value is called");
                $("#typeCWeb").siblings('.mdl-textfield__error_msg').css("display", "none");
            }
            alert("complaintsWR" + webReturn);
        }


        // Concern Options Start
        function faceConcern() {
            if ($('#faceCon').val().length == 0) {
                $("#faceCon").siblings('.mdl-textfield__error_msg').css("display", "block");
                webReturn = false;
            } else {
                $("#faceCon").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            alert("faceConcern " + webReturn);
        }

        // Web Comments Start
        function webComments() {
            if ($('#sample5').val().length == 0) {
                $("#sample5").siblings('.mdl-textfield__error_msg').css("display", "block");
                webReturn = false;
            } else {
                $("#sample5").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            alert("webComments " + webReturn);
        }



    });

    $("#sample5").on('keyup', function() {
        //alert("Make a Complaint Comments Called");
        var words = this.value.length;
        if (words > 250) {
            var trimmed = $(this).val();
            $(this).val(trimmed + " ");
        } else {
            $('.coutNum').text(250 - words);
        }
    });

    /* Prepaid Make a Complaint | Web Related  End*/


    /*Make a Complaint | Recharge Benefits Not Received Start */

    $("#rbnrRechage,#benefitsWebForm #amount,#beneRechage , #benefitsWebForm #comments").on("keyup keydown input", function(event) {
        if (($(this).val().length >= 0)) {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");
        } else {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");
        }

    });


    $("#mofRcge").on("change input", function(event) {
        if (($(this).val().length == 0) && ($(this).val() == "")) {
            console.log("value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");

        } else {
            console.log(" Else value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");

        }
    });


    $("#prebnRConfirm").on("click", function() {
        console.log("Make a Complaint | Recharge Benefits Not Received is called");
        var rbnRReturn = true;
        complaintsRD();
        modeofNRechage();
        rechageAmountC();
        rechargeBenefits();
        rbnComments();

        if (rbnRReturn === true) {
            console.log("Valid is called");
            alert("form is sbumiited Valid is called");
        } else {
            alert("Not is form is sbumiited Valid is called");
            console.log("invalid");
            return false;
        }

        // Prepaid Recharge Data
        function complaintsRD() {
            if ($('#rbnrRechage').val().length == 0) {
                $("#rbnrRechage").siblings('.mdl-textfield__error_msg').css("display", "block");
                rbnRReturn = false;
            } else {
                $("#rbnrRechage").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            // alert("complaintsRD " + rbnRReturn);
        }

        //Mode of recharge
        function modeofNRechage() {
            if (($("#mofRcge").val().length == 0) && ($("#mofRcge").val() == "")) {
                console.log("value is called");
                $("#mofRcge").siblings('.mdl-textfield__error_msg').css("display", "block");
                rbnRReturn = false;
            } else {
                console.log(" Else value is called");
                $("#mofRcge").siblings('.mdl-textfield__error_msg').css("display", "none");
            }
            // alert("modeofNRechage " + rbnRReturn);
        }




        // Recharge Amount Start
        function rechageAmountC() {
            if ($('#benefitsWebForm #amount').val().length == 0) {
                $("#benefitsWebForm #amount").siblings('.mdl-textfield__error_msg').css("display", "block");
                rbnRReturn = false;
            } else {
                $("#benefitsWebForm #amount").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            //alert("rechageAmountC " + rbnRReturn);
        }

        //benefits promised on the recharge

        function rechargeBenefits() {
            if ($('#beneRechage').val().length == 0) {
                $('#beneRechage').siblings('.mdl-textfield__error_msg').css("display", "block");
                rbnRReturn = false;
            } else {
                $('#beneRechage').siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            //alert("rechargeBenefits " + rbnRReturn);
        }





        // Web Comments Start
        function rbnComments() {
            if ($('#benefitsWebForm #comments').val().length == 0) {
                $('#benefitsWebForm #comments').siblings('.mdl-textfield__error_msg').css("display", "block");
                rbnRReturn = false;
            } else {
                $('#benefitsWebForm #comments').siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            //alert("rbnComments " + rbnRReturn);
        }


    });





    $("#benefitsWebForm #comments").on('keypress', function() {
        //alert("Make a Complaint Comments Called");
        var words = this.value.length;
        if (words > 250) {
            var trimmed = $(this).val();
            $(this).val(trimmed + " ");
        } else {
            $('.coutNum').text(250 - words);
        }
    });

    $("#benefitsWebForm #amount").on('input', function() {
        console.log("Amount is called");
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
    });



    /*Make a Complaint | Recharge Benefits Not Received End */


    /* Make a Complaint | Data/GPRS/MMS Not Working Start*/









    $("#handsetM,#duProb,#beneRechage , #preExactLoc, #altCNumber,#netGPRSComments").on("keyup keydown input", function(event) {
        if (($(this).val().length >= 0)) {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");
        } else {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");
        }

    });


    $("#mobConern,#signalVisi").on("change", function(event) {
        if (($(this).val().length == 0) && ($(this).val() == "")) {
            console.log("value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");

        } else {
            console.log(" Else value is called");
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");

        }
    });


    $("#gprsForm #rContactNumber").on("keyup input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 10 && (!isNaN(tempVal)) && (event.type = "input")) {
            $(this).closest('.cmobNumberInput').find('.mdl-textfield__error_msg').hide();
            //validNumber();
        } else if (tempVal.length > 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if ((this.value.length > 0 && this.value.length < 10) && (event.keyCode == 8 || event.keyCode == 46)) {
            //invalidNumber();
            $(this).closest('.cmobNumberInput').find('.mdl-textfield__error_msg').show();
        } else {
            // invalidNumber();
            $(this).closest('.cmobNumberInput').find(".mdl-textfield__error_msg").css("display", "none");
            if ($(".cmobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $(".cmobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            if ($(".cmobNumberInput #validationIcon").hasClass("validNumber")) {
                $(".cmobNumberInput #validationIcon").removeClass("validNumber");
            }
        }
    });


    $("#gprsForm #netGPRSComments").on('keyup', function() {
        //alert("Make a Complaint Comments Called");
        var words = this.value.length;
        if (words > 250) {
            var trimmed = $(this).val();
            $(this).val(trimmed + " ");
        } else {
            $('.coutNum').text(250 - words);
        }
    });


    $('#gprsForm .mdl-radio__button').on("change", function() {
        console.log("Radio button is prepaid coverage is called");
        gprsResetForm();
    });


    function gprsResetForm() {
        $("#duProb").val('');
        $(".mdl-textfield__error_msg").hide();
        $(".textfield__error").hide();
        $('#mobConern option:first').prop('selected', true);
        $('#signalVisi option:first').prop('selected', true);
        $("#handsetM").val('');
        $("#preExactLoc").val('');
        $("#rContactNumber").val('');
        $("#netGPRSComments").val('');
        $(".coutNum").text('250');
        $("#gprsForm .mdl-textfield").removeClass("is-dirty");
    }

    $("#preGprsDone").on("click", function() {
        console.log("Make a Complaint | Data/GPRS/MMS Not Working");
        var gprsReturn = true;
        comGPRS();
        gprsSignalVisble();
        gprshandsetModel();
        gprsDuProb();
        gprsProLoc();
        gprsAltConNum();
        gprsComments();

        if (gprsReturn === true) {
            console.log("Valid is called");
            alert("GPRS form is sbumiited Valid is called");
        } else {
            alert("GPRS form NOT is sbumiited Valid is called");
            console.log("invalid");
            return false;
        }

        //Type of Concern GPRS		
        function comGPRS() {
            if (($("#mobConern").val().length == 0) && ($("#mobConern").val() == "")) {
                console.log("value is called");
                $("#mobConern").siblings('.mdl-textfield__error_msg').css("display", "block");
                gprsReturn = false;
            } else {
                console.log(" Else value is called");
                $("#mobConern").siblings('.mdl-textfield__error_msg').css("display", "none");
            }
            console.log("comGPRS " + gprsReturn);
        }

        // Number of signal bars visible 
        function gprsSignalVisble() {
            if (($("#signalVisi").val().length == 0) && ($("#signalVisi").val() == "")) {
                console.log("value is called");
                $("#signalVisi").siblings('.mdl-textfield__error_msg').css("display", "block");
                gprsReturn = false;
            } else {
                console.log(" Else value is called");
                $("#signalVisi").siblings('.mdl-textfield__error_msg').css("display", "none");
            }
            console.log("gprsSignalVisble " + gprsReturn);
        }


        // Handset Model & Number Start
        function gprshandsetModel() {
            if ($('#handsetM').val().length == 0) {
                $("#handsetM").siblings('.mdl-textfield__error_msg').css("display", "block");
                gprsReturn = false;
            } else {
                $("#handsetM").siblings(".mdl-textfield__error_msg").css("display", "none");
            }

            console.log("gprshandsetModel" + gprsReturn);
        }


        //Duration Of problem (e.g Days,Weeks,Months)

        function gprsDuProb() {
            if ($('#duProb').val().length == 0) {
                $("#duProb").siblings('.mdl-textfield__error_msg').css("display", "block");
                gprsReturn = false;
            } else {
                $("#duProb").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            console.log("gprsDuProb   " + gprsReturn);
        }


        // Exact address location Start

        function gprsProLoc() {
            if ($('#preExactLoc').val().length == 0) {
                $("#preExactLoc").siblings('.mdl-textfield__error_msg').css("display", "block");
                gprsReturn = false;
            } else {
                $("#preExactLoc").siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            console.log("gprsProLoc " + gprsReturn);
        }


        // Alternet Contact Number
        function gprsAltConNum() {
            if ($('#gprsForm #rContactNumber').val().length == 10) {
                $("#gprsForm #rContactNumber").siblings("#textfield__error_mobileNo").hide();
                // $('#gprsForm #rContactNumber').parent().removeClass("textfield__error");


            } else {
                //  $('#rContactNumber').siblings(".mdl-textfield__error").show();
                // $('#gprsForm #rContactNumber').parent().addClass("textfield__error");
                $("#gprsForm #rContactNumber").siblings(".mdl-textfield__error_msg").css("display", "block");
                gprsReturn = false;
            }
            console.log("gprsAltConNum " + gprsReturn);
        }


        // GPRS Comments Start
        function gprsComments() {
            if ($('#netGPRSComments').val().length == 0) {
                $('#netGPRSComments').siblings('.mdl-textfield__error_msg').css("display", "block");
                gprsReturn = false;
            } else {
                $('#netGPRSComments').siblings(".mdl-textfield__error_msg").css("display", "none");
            }
            console.log("gprsComments" + gprsReturn);
        }



    });


    /* Make a Complaint | Data/GPRS/MMS Not Working End*/


    /* var rowCount = $('.selfcare-table tbody tr').length;
     console.log(rowCount);
     $(".one").append(rowCount + " request"); */


    $(".dialogMSmall").attr("autocomplete", "off");



});

/* Login Validation Start */
$(document).ready(function() {
    // $("#LoginForm #loginMobileNumber").focus();
    $("#LoginForm #loginMobileNumber").on("input", function(event) {
        var tempVal = $(this).val();
        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 10 && (!isNaN(tempVal))) {
            $("#LoginForm #loginMobileNumber").next(".validation").remove(); // remove it
            if ($("#LoginForm .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#LoginForm .mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
            if (!$("#LoginForm .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#LoginForm .mobNumberInput #validationIcon").addClass("validNumber");
            }

        } else if (tempVal.length >= 10) {
            $(this).val($(this).val().substring(0, 10) + '');
        } else if (tempVal.length > 0 && tempVal.length < 10 && (event.keyCode == 8 || event.keyCode == 46)) {
            if ($("#LoginForm .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#LoginForm .mobNumberInput #validationIcon").removeClass("validNumber");
            }
            if (!$("#LoginForm .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#LoginForm .mobNumberInput #validationIcon").addClass("invalidNumber");
            }
        } else {
            if ($("#LoginForm .mobNumberInput #validationIcon").hasClass("validNumber")) {
                $("#LoginForm .mobNumberInput #validationIcon").removeClass("validNumber");
            }
            if ($("#LoginForm .mobNumberInput #validationIcon").hasClass("invalidNumber")) {
                $("#LoginForm .mobNumberInput #validationIcon").removeClass("invalidNumber");
            }
        }
    });
    $("#LoginForm #loginMobileNumber").on("blur", function(event) {
        if (this.value.length < 10 && this.value.length >= 0) {
            if (!($("#LoginForm .mobNumberInput #validationIcon").hasClass("invalidNumber"))) {
                $("#LoginForm .mobNumberInput #validationIcon").addClass("invalidNumber");
                if (!$("#LoginForm #loginMobileNumber").siblings("span").hasClass("validation")) {
                    $("#LoginForm #loginMobileNumber").after("<span class='validation'>Enter 10 digit Mobile Number</span>");
                }
            }
        } else if (this.value.length == 10) {
            $("#LoginForm #loginMobileNumber").next(".validation").remove(); // remove it
        }
    });

    $("#password-otp").on("input", function(event) {

        if (!$('#LoginForm #loginMobileNumber').val()) {
            if ($("#LoginForm #loginMobileNumber").next(".validation").length == 0) // only add if not added
            {
                if (!$("#LoginForm #loginMobileNumber").siblings("span").hasClass("validation")) {
                    $("#LoginForm #loginMobileNumber").after("<span class='validation'>Enter 10 digit Mobile Number</span>");
                }
            }
            toReturn = false;
            $("#LoginForm #loginMobileNumber").focus();
        }
        var tempVal = $(this).val();
        if (tempVal.length == 4 && !this.value.match(/^\d+$/)) {
            alert("invalid otp 1");
            $("#LoginForm #password-otp").focus();
            $("#LoginForm .invalidPass").hide();
            $("#LoginForm .invalidOTPPass").show();
        } else if (tempVal.length == 4 && this.value.match(/^\d+$/)) {
            alert("Valid OTP 1");
            $("#LoginForm .invalidPass").hide();
            $("#LoginForm .invalidOTPPass").hide();
        } else {
            $("#LoginForm #password-otp").focus();
            $("#LoginForm .invalidPass").hide();
            $("#LoginForm .invalidOTPPass").show();
        }
    });


    $("#handsetM, #duProb, #beneRechage ,#preExactLoc, #altCNumber,#netGPRSComments").on("keyup keydown input", function(event) {
        if (($(this).val().length >= 0)) {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "none");
        } else {
            $(this).siblings('.mdl-textfield__error_msg').css("display", "block");
        }

    });



});

$('#loginBtn').click(function() {
    if ($("#LoginForm #password-otp").val().length < 4) {
        $("#LoginForm .invalidPass").hide();
        $("#LoginForm .invalidOTPPass").show();
        $("#LoginForm #password-otp").focus();
    } else if ($("#LoginForm #password-otp").val().length > 4 && $("#LoginForm #password-otp").val().length < 8) {
        //alert("Invalid Password");
        $("#LoginForm .invalidPass").show();
        $("#LoginForm .invalidOTPPass").hide();
        $("#LoginForm #password-otp").focus();
    } else if ($("#LoginForm #password-otp").val().length < 4) {
        // alert("Please Enter Valid OTP/Password");
        $("#LoginForm .invalidPass").hide();
        $("#LoginForm .invalidOTPPass").show();
        $("#LoginForm #password-otp").focus();
    } else if ($("#LoginForm #password-otp").val().length >= 8) {
        $("#LoginForm .invalidPass").hide();
        $("#LoginForm .invalidOTPPass").hide();
    } else if ($("#LoginForm #password-otp").val().length > 0 && $("#LoginForm #password-otp").val().length < 4 && (event.keyCode == 8 || event.keyCode == 46)) {
        // alert("Please Enter Valid OTP/Password");
        $("#LoginForm .invalidPass").hide();
        l
        $("#LoginForm .invalidOTPPass").show();
        $("#LoginForm #password-otp").focus();
    } else if ($("#LoginForm #password-otp").val().length == 4 && (!$("#LoginForm #password-otp").val().match(/^\d+$/))) {
        alert("invalid otp 12535");
        $("#LoginForm #password-otp").focus();
        $("#LoginForm .invalidPass").hide();
        $("#LoginForm .invalidOTPPass").show();
    }
    if ($("#LoginForm #loginMobileNumber").val().length < 10) {
        $("#LoginForm #loginMobileNumber").focus();
        if (!$("#LoginForm #loginMobileNumber").siblings("span").hasClass("validation")) {
            $("#LoginForm #loginMobileNumber").after("<span class='validation'>Enter 10 digit Mobile Number</span>");
        }
    } else if ($("#LoginForm #loginMobileNumber").val().length == 10) {
        $("#LoginForm #loginMobileNumber").next(".validation").remove(); // remove it
    }

    $("#password-otp").on("input", function(event) {
        var tempVal = $(this).val();
        //        this.value = this.value.replace(/[^0-9]/g, '');
        if (tempVal.length == 4 && this.value.match(/^\d+$/)) {
            // alert("Valid OTP");
            $("#LoginForm .invalidPass").hide();
            $("#LoginForm .invalidOTPPass").hide();
        } else if (tempVal.length > 0 && tempVal.length < 4 && (event.keyCode == 8 || event.keyCode == 46)) {
            // alert("Please Enter Valid OTP/Password");
            $("#LoginForm .invalidPass").hide();
            $("#LoginForm .invalidOTPPass").show();
            $("#LoginForm #password-otp").focus();
        } else if (tempVal.length > 4 && tempVal.length < 8) {
            //alert("Invalid Password");
            $("#LoginForm .invalidPass").show();
            $("#LoginForm .invalidOTPPass").hide();
            $("#LoginForm #password-otp").focus();
        } else if (tempVal.length < 4) {
            // alert("Please Enter Valid OTP/Password");
            $("#LoginForm .invalidPass").hide();
            $("#LoginForm .invalidOTPPass").show();
            $("#LoginForm #password-otp").focus();
        } else if (tempVal.length >= 8) {
            $("#LoginForm .invalidPass").hide();
            $("#LoginForm .invalidOTPPass").hide();
        }
    });

});
/* Login Validation End  */